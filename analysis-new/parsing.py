import os
import copy
import pandas as pd
import numpy as np

TO=120

def iterate_logs(directory):
    for file in os.listdir(directory):
        log_content = []
        net_name = None
        spec_name = os.path.basename(file).split(".")[0]
        if not file.endswith(".log"):
            continue
        with open(os.path.join(directory, file), "r") as f:
            for line in f:
                if line.startswith("[VERYDIFF_EXPERIMENT] Running on "):
                    if net_name is not None:
                        yield (spec_name, net_name, log_content)
                    log_content = []
                    net_name = os.path.basename(line.split()[-1]).split('.')[0]
                else:
                    log_content.append(line)
            if net_name is not None:
                yield (spec_name, net_name, log_content)

def iterate_logs_confidence(directory):
    for file in os.listdir(directory):
        log_content = []
        log_file_name = ".".join(file.split(".")[:-1])
        if not file.endswith(".log"):
            continue
        confidence = log_file_name.split("-")[-1]
        pruning = log_file_name.split("-")[-2]
        net_name = "-".join(log_file_name.split("-")[:-2])
        sigma = None
        with open(os.path.join(directory, file), "r") as f:
            for line in f:
                if line.startswith("[VERYDIFF_EXPERIMENT] Sigma:"):
                    if sigma is not None:
                        yield (net_name, pruning, confidence, sigma, log_content)
                    log_content = []
                    sigma = line.split()[-1]
                else:
                    log_content.append(line)
            if sigma is not None:
                yield (net_name, pruning, confidence, sigma, log_content)

def parse_log_file(directory, solver, property, mirror, parser):
    data = []
    for spec_name, net_name, log_content in iterate_logs(directory):
        cur_data = parser(log_content)
        cur_data["network"] = net_name
        cur_data["spec"] = spec_name
        data.append(cur_data)
    pd_data = pd.DataFrame(data)
    pd_data["solver"] = solver
    pd_data["property"] = property
    pd_data["mirror"] = mirror
    return pd_data

def parse_log_dirs(directories, solver, property, parser):
    results = []
    for (directory, mirror) in directories:
        results.append(parse_log_file(directory, solver, property, mirror, parser))
    return pd.concat(results)

def parse_log_dirs_confidence(directory, solver, parser):
    data = []
    for net_name, pruning, confidence, sigma, log_content in iterate_logs_confidence(directory):
        cur_data = parser(log_content)
        cur_data["network"] = net_name
        cur_data["sigma"] = float(sigma)
        cur_data["delta"] = float(confidence)
        cur_data["spec"] = sigma+"-"+confidence
        cur_data["mirror"] = "prune-"+pruning
        data.append(cur_data)
    pd_data = pd.DataFrame(data)
    pd_data["solver"] = solver
    pd_data["property"] = "delta-top-1"
    return pd_data

def parse_neurodiff(log_lines):
    benchmark_data = {}
    benchmark_data["status"] = "unknown"
    benchmark_data["time"] = 120
    zono_bounds_incoming = False
    zono_bounds_i = 0
    for line in log_lines:
        if line.startswith("Initial output delta:"):
            zono_bounds_incoming = True
            continue
        if zono_bounds_incoming:
            if zono_bounds_i<2:
                bounds = np.array([float(x) for x in (line.strip())[1:-1].strip().split(" ")])
                benchmark_data["init_bounds_"+str(zono_bounds_i)] = bounds
                zono_bounds_i += 1
            else:
                zono_bounds_incoming = False
        parts = line.split()
        if line.startswith("No adv!"):
            benchmark_data["status"] = "safe"
        elif line.startswith("adv found"):
            benchmark_data["status"] = "unsafe"
        elif "[VERYDIFF_TIMER]" in line:
            benchmark_data["time"] = float(parts[-1])
        #elif line.startswith("adv is: "):
        #    benchmark_data["cex"] = line[8:]
        elif "TIMEOUT" in line:
            benchmark_data["status"] = "unknown"
            benchmark_data["time"] = 120
    return benchmark_data

def parse_verydiff(log_lines):
    benchmark_data = {}
    benchmark_data["status"] = "safe"
    benchmark_data["time"] = TO
    found_unsafe = False
    zono_bounds_incoming = False
    zono_bounds_i = 0
    instable_generators = 0
    for line in log_lines:
        if line.startswith("Zono Bounds:"):
            zono_bounds_incoming = True
            continue
        if zono_bounds_incoming:
            if zono_bounds_i<2:
                bounds = np.array([float(x) for x in (line.strip())[1:-1].split(", ")])
                benchmark_data["init_bounds_"+str(zono_bounds_i)] = bounds
                zono_bounds_i += 1
            else:
                zono_bounds_incoming = False
        if line.startswith("Found counterexample"):
            benchmark_data["status"] = "unsafe"
            found_unsafe = True
        elif line.startswith("Instable Generators:"):
            instable_generators += int(line.split()[-1])
        elif "Found cex" in line:
            benchmark_data["status"] = "unsafe"
            found_unsafe = True
        elif "[VERYDIFF_TIMER]" in line:
            parts = line.split()
            benchmark_data["time"] = float(parts[-1])
        elif line.startswith("[Thread 1] Total splits:"):
            splits = int(line.split(" ")[-1])
            benchmark_data["splits"] = splits
        elif line.startswith("TIMEOUT REACHED"):
            benchmark_data["status"] = "unknown"
            benchmark_data["time"] = 120
        elif "exception" in line.lower() or "error" in line.lower():
            if not found_unsafe:
                benchmark_data["status"] = "unknown"
                benchmark_data["time"] = 120
            elif not "BoundsError" in line:
                # We ignore BoundsError exceptions after a counterexample was found
                # Bug in VeryDiff has been fixed since and was not soundness critical
                benchmark_data["status"] = "unknown"
                benchmark_data["time"] = 120
    benchmark_data["instable_generators"] = instable_generators
    return benchmark_data

def parse_verydiff_confidence(log_lines):
    benchmark_data = {}
    benchmark_data["status"] = "safe"
    benchmark_data["time"] = TO
    benchmark_data["spurious"] = 0
    benchmark_data["difference"] = 0.0
    found_unsafe = False
    zono_bounds_incoming = False
    zono_bounds_i = 0
    instable_generators = 0
    for line in log_lines:
        if line.startswith("Zono Bounds:"):
            zono_bounds_incoming = True
            continue
        if zono_bounds_incoming:
            if zono_bounds_i<2:
                bounds = np.array([float(x) for x in (line.strip())[1:-1].split(", ")])
                benchmark_data["init_bounds_"+str(zono_bounds_i)] = bounds
                zono_bounds_i += 1
            else:
                zono_bounds_incoming = False
        if line.startswith("Found counterexample"):
            benchmark_data["status"] = "unsafe"
            found_unsafe = True
        elif line.startswith("Distance"):
            benchmark_data["difference"] += float(line.split(" ")[-1])
        elif line.startswith("Instable Generators:"):
            instable_generators += int(line.split()[-1])
        elif "Found cex" in line:
            benchmark_data["status"] = "unsafe"
            found_unsafe = True
        elif "[VERYDIFF_TIMER]" in line:
            parts = line.split()
            benchmark_data["time"] = float(parts[-1])
        elif line.startswith("[Thread 1] Total splits:"):
            splits = int(line.split(" ")[-1])
            benchmark_data["splits"] = splits
        elif line.startswith("TIMEOUT REACHED"):
            benchmark_data["status"] = "unknown"
            benchmark_data["time"] = 120
        elif "exception" in line.lower() or "error" in line.lower():
            if not found_unsafe:
                benchmark_data["status"] = "unknown"
                benchmark_data["time"] = 120
            elif not "BoundsError" in line:
                # We ignore BoundsError exceptions after a counterexample was found
                # Bug in VeryDiff has been fixed since and was not soundness critical
                benchmark_data["status"] = "unknown"
                benchmark_data["time"] = 120
        elif line.startswith("Found spurious cex"):
            benchmark_data["spurious"] += 1
    benchmark_data["instable_generators"] = instable_generators
    return benchmark_data

def parse_nnequiv(log_lines):
    benchmark_data = {}
    benchmark_data["status"] = "unknown"
    benchmark_data["time"] = 120
    found_unsafe = False
    for line in log_lines:
        if line.startswith("NETWORKS EQUIVALENT"):
            benchmark_data["status"] = "safe"
        elif line.startswith("UNKNOWN") or line.startswith("TIMEOUT"):
            benchmark_data["status"] = "unknown"
            benchmark_data["time"] = 120
        elif line.startswith("NETWORKS NOT EQUIVALENT"):
            benchmark_data["status"] = "unsafe"
        elif line.startswith("[VERYDIFF_TIMER]"):
            parts = line.split()
            benchmark_data["time"] = float(parts[1])

    return benchmark_data

def parse_milpequiv(log_lines):
    benchmark_data = {}
    benchmark_data["status"] = "safe"
    benchmark_data["time"] = 120
    found_unsafe = False
    for line in log_lines:
        if line.startswith("NETWORKS EQUIVALENT"):
            benchmark_data["status"] = "safe"
        elif line.startswith("UNKNOWN") or line.startswith("TIMEOUT"):
            benchmark_data["status"] = "unknown"
            benchmark_data["time"] = 120
        elif line.startswith("NETWORKS NOT EQUIVALENT"):
            benchmark_data["status"] = "unsafe"
        elif line.startswith("[VERYDIFF_TIMER]"):
            parts = line.split()
            benchmark_data["time"] = float(parts[1])

    return benchmark_data

