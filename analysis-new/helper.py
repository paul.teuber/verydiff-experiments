import numpy as np
def compute_speedup(df, variant, baseline):
    if not "speedup" in df.columns:
        df["speedup"] = np.nan
    no_to_baseline = df[np.logical_and(df['solver'] == baseline, df['status'] != 'unknown')].copy()
    no_to_variant = df[np.logical_and(df['solver'] == variant, df['status'] != 'unknown')].copy()
    print("Baseline: ", len(no_to_baseline))
    print("Variant: ", len(no_to_variant))
    merged = no_to_baseline.merge(no_to_variant, on=['spec', 'network', 'mirror'], suffixes=('_baseline', '_variant'))
    for _, row in merged[merged['status_baseline'] != merged['status_variant']].iterrows():
        print(f"Warning: status mismatch for {row['spec']} {row['network']} {row['mirror']} ({row['status_baseline']} vs {row['status_variant']})")
        print(f"results_full[(results_full['solver']=='{baseline}') & (results_full['spec']=='{row['spec']}') & (results_full['network']=='{row['network']}') & (results_full['mirror']=='{row['mirror']}'), 'status'] = 'unknown'")
    print("Merged: ", len(merged))
    merged['speedup'] = merged['time_baseline'] / merged['time_variant']
    merged['solver'] = merged['solver_baseline']
    df = df.merge(merged[['spec', 'network', 'mirror', 'solver', 'speedup']], on=['spec', 'network', 'mirror','solver'],how='left', suffixes=('_x', '_y'))
    df['speedup'] = df['speedup_y'].combine_first(df['speedup_x'])
    df.drop(['speedup_x', 'speedup_y'], axis=1, inplace=True)
    return df

def compute_speedup_confidence(df, variant, baseline):
    if not "speedup" in df.columns:
        df["speedup"] = np.nan
        df["split_reduction"] = np.nan
    no_to_baseline = df[np.logical_and(df['solver'] == baseline, df['status'] != 'unknown')].copy()
    no_to_variant = df[np.logical_and(df['solver'] == variant, df['status'] != 'unknown')].copy()
    print("Baseline: ", len(no_to_baseline))
    print("Variant: ", len(no_to_variant))
    merged = no_to_baseline.merge(no_to_variant, on=['spec', 'network', 'mirror'], suffixes=('_baseline', '_variant'))
    for _, row in merged[merged['status_baseline'] != merged['status_variant']].iterrows():
        print(f"Warning: status mismatch for {row['spec']} {row['network']} {row['mirror']} ({row['status_baseline']} vs {row['status_variant']})")
        print(f"results_full[(results_full['solver']=='{baseline}') & (results_full['spec']=='{row['spec']}') & (results_full['network']=='{row['network']}') & (results_full['mirror']=='{row['mirror']}'), 'status'] = 'unknown'")
    print("Merged: ", len(merged))
    merged['speedup'] = merged['time_baseline'] / merged['time_variant']
    merged['split_reduction'] = merged['splits_variant'] / merged['splits_baseline']
    merged['solver'] = merged['solver_baseline']
    df = df.merge(merged[['spec', 'network', 'mirror', 'solver', 'speedup', 'split_reduction']], on=['spec', 'network', 'mirror','solver'],how='left', suffixes=('_x', '_y'))
    df['speedup'] = df['speedup_y'].combine_first(df['speedup_x'])
    df['split_reduction'] = df['split_reduction_y'].combine_first(df['split_reduction_x'])
    df.drop(['speedup_x', 'speedup_y','split_reduction_x','split_reduction_y'], axis=1, inplace=True)
    return df

def compute_increase(df, variant, baseline, column):
    if not f"increase-{column}" in df.columns:
        df[f"increase-{column}"] = np.nan
    working_copy_variant = df[df["solver"]==variant].copy()
    working_copy_baseline = df[df["solver"]==baseline].copy()
    merged = working_copy_baseline.merge(working_copy_variant, on=['property','family'], suffixes=('_baseline', '_variant'))
    
    merged["increase-{column}"] = merged[f"{column}_baseline"] / merged[f"{column}_variant"]
    merged['solver'] = merged['solver_baseline']
    df = df.merge(merged[['property','family', 'solver', "increase-{column}"]], on=['property','family', 'solver'],how='left', suffixes=('_x', '_y'))
    return df

def q25(x):
    return x.quantile(0.25)

def q75(x):
    return x.quantile(0.75)