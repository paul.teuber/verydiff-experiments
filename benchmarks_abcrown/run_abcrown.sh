#!/bin/bash

ABCROWN_PATH="../../abCROWN/complete_verifier/abcrown.py"

source "$(conda info --base)/etc/profile.d/conda.sh"
conda activate alpha-beta-crown

# run abCROWN and ensure that it is single threaded
taskset -c 0 python "$ABCROWN_PATH" --config acasxu_modified.yaml
taskset -c 0 python "$ABCROWN_PATH" --config mnistfc_modified.yaml
taskset -c 0 python "$ABCROWN_PATH" --config lhc_config.yaml
