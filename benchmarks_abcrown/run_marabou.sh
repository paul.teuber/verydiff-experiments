#!/bin/bash

MARABOU_PATH=/opt/conda/bin/Marabou
#MARABOU_PATH=echo

# mkdir -p marabou_results/{acas,mnist}

# Iterate over all benchmark instances in ./acas-prune/instances.csv
while IFS=, read -r benchmark_net benchmark_spec benchmark_timeout
do
    benchmark_net=$(echo $benchmark_net | tr -d '[:blank:]')
    benchmark_spec=$(echo $benchmark_spec | tr -d '[:blank:]')
    echo "Running Marabou on $benchmark_net with spec $benchmark_spec and timeout $benchmark_timeout"
    net_name=$(basename $benchmark_net)
    spec_name=$(basename $benchmark_spec)
    { time $MARABOU_PATH ./acas-prune/$benchmark_net ./acas-prune/$benchmark_spec --timeout $benchmark_timeout; } > marabou_results/acas/$net_name-$spec_name.log 2>&1
done < ./acas-prune/instances.csv

# Iterate over all benchmark instances in ./mnist-prune/instances.csv
while IFS=, read -r benchmark_net benchmark_spec benchmark_timeout
do
    benchmark_net=$(echo $benchmark_net | tr -d '[:blank:]')
    benchmark_spec=$(echo $benchmark_spec | tr -d '[:blank:]')
    benchmark_timeout=$(echo $benchmark_timeout | tr -d '[:blank:]')
    echo "Running Marabou on $benchmark_net with spec $benchmark_spec and timeout $benchmark_timeout"
    net_name=$(basename $benchmark_net)
    spec_name=$(basename $benchmark_spec)
    { time timeout 120 $MARABOU_PATH ./mnist-prune/$benchmark_net ./mnist-prune/$benchmark_spec; } > marabou_results/mnist/$net_name-$spec_name.log 2>&1
done < ./mnist-prune/filtered_instances.csv


