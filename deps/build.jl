using Pkg
using Pkg.Artifacts

using Conda

ENV["PYTHON"]=""
println("Building PyCall...")

Pkg.build("PyCall")
Pkg.build("Conda")
#Conda.clean()
Conda.add("python=3.9")
Conda.pip_interop(true)
Pkg.build("PyCall")

Conda.pip("install", "gurobipy")
Conda.pip("install", "h5py")

println("Installing dependencies of NNEquiv...")
Conda.pip("install -r", artifact"nnequiv"*"/nnequiv-ffc42dbd08a11277dac65b55db731d2ffb66bad9/requirements.txt")