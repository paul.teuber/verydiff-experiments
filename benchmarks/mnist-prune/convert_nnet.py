from NNet.converters.onnx2nnet import onnx2nnet
from NNet.python.nnet import NNet
from onnxruntime import InferenceSession
import numpy as np

import onnx
import os

def test_equivalence(onnx_file, nnet_file):
    print("# Testing equivalence of "+onnx_file+" and "+nnet_file)
    nnet_model = NNet(nnet_file)
    num_in = nnet_model.num_inputs()
    sess = InferenceSession(onnx_file)
    for i in range(0,100):
        input_shape = sess.get_inputs()[0].shape
        #print("Input shape: "+str(input_shape))
        x = np.float32(np.random.rand(*input_shape))
        onnx_val = sess.run(None, {"X": x})[0].flatten()
        nnet_val = nnet_model.evaluate_network(x.flatten()).flatten()
        if not np.allclose(onnx_val, nnet_val, atol=1e-3):
            print("Outputs do not match for input "+str(x))
            print("ONNX: "+str(onnx_val))
            print("NNet: "+str(nnet_val))
            assert False


def convert_in_dir(cur_dir):
    # Convert ONNX to NNet
    for file in os.listdir(cur_dir):
        if file.endswith(".onnx") and not file.endswith("-old.onnx"):
            print("# Converting "+file+" to NNet")
            onnx2nnet(cur_dir+file)
            test_equivalence(cur_dir+file, cur_dir+file[:-5]+".nnet")

# Get path of file
dir1 = os.path.dirname(os.path.realpath(__file__))+"/"+"nets/"
dir2 = os.path.dirname(os.path.realpath(__file__))+"/"+"nets_pruned/"

# Convert ONNX to NNet
convert_in_dir(dir1)
#convert_in_dir(dir2)