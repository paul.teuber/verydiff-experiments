Contrary to the folder names, these nets are not actually pruned, but instead weightmatched.
We just use this naming to reuse the existing automation because the author was unfortunately lazy at this particular point in time.

In particular mnist_512_1.onnx === mnist_512_wm.onnx