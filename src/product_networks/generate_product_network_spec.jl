
using VNNLib


function write_declare_vars(n_in::Integer, n_out::Integer, filename)
    # create new file here, since declare vars is always at the beginning 
    # other methods have "a" to append instead of "w" to write new file
    open(filename, "w") do f
        for i in 0:n_in-1
            println(f, "(declare-const X_", i, " Real)")
        end

        for i in 0:n_out-1
            println(f, "(declare-const Y_", i, " Real)")
        end
    end
end


function write_input_constraints(lbs::AbstractVector, ubs::AbstractVector, filename)
    open(filename, "a") do f
        println(f, "\n; Input constraints")
        for (i, (lᵢ, uᵢ)) in enumerate(zip(lbs, ubs))
            println(f, "(assert (>= X_", i-1, " ", lᵢ, "))")
            println(f, "(assert (<= X_", i-1, " ", uᵢ, "))")
        end
    end
end


function write_eps_constraint(n_out::Integer, eps::Real, filename)
    open(filename, "a") do f
        println(f, "\n; Output constraints")
        # satisfied output constraints => counterexample found
        # SAT if at least one of the outputs is <= -eps or >= eps 
        println(f, "(assert (or")
        for i in 0:n_out-1
            println(f, "\t(and (>= Y_", i, " ", eps, "))")
            println(f, "\t(and (<= Y_", i, " ", -eps, "))")
        end
        println(f, "))")
    end
end


"""

args:
    n_out - number of output neurons of the **original network**, not the product network
            (e.g. MNIST has 10 outputs, the product network will have 20 outputs, n_out = 10 is correct)
"""
function write_top1_constraint(n_out::Integer, filename)
    open(filename, "a") do f
        println(f, "\n; Output constraints")
        # satisfied output constraints => counterexample found
        # SAT if NN1 has a unique maximum that is not the maximum of NN2
        println(f, "(assert (or")
        for i in 0:n_out-1
            # i is max output of NN1
            println(f, "\t(and")
            for j in 0:n_out-1
                if j != i
                    println(f, "\t\t(>= Y_", i, " Y_", j, ")")
                end
            end

            # k is larger than i in NN2
            println(f, "\t\t(or")
            for k in 0:n_out-1
                if k != i
                    println(f, "\t\t\t(>= Y_", k+n_out, " Y_", i+n_out, ")")
                end
            end
            println(f, "\t))")
        end
        println(f, "))")
    end
end


"""
Appends constraints to check δ-top-1 equivalence to the given vnnlib specification file.

Since most solvers require vnnlib formulas to be in disjunctive normal form, we convert the 
shorter formulation of

    ⋁ᵢ ((⋀ⱼ≠ᵢ xᵢ ≥ xⱼ + T) ∧ (⋁ⱼ≠ᵢ yⱼ ≥ yᵢ))

to the DNF formulation

    ⋁ᵢ ⋁ₖ≠ᵢ ((⋀ⱼ≠ᵢ xᵢ ≥ xⱼ + T)  ∧ yₖ ≥ yᵢ)

(note that we don't require yⱼ to be the actual classification result, if it is 
larger than yᵢ it is already sufficient to be a counterexample to yᵢ being the classification result)

args:
    n_out - number of output neurons of the **original network**, not the product network
            (e.g. MNIST has 10 outputs, the product network will have 20 outputs, n_out = 10 is correct)
    δ - confidence threshold for the classification result in the reference network 
    filename - vnnlib file to append to 
"""
function write_δ_top1_constraint(n_out::Integer, δ::Number, filename)
    T = log(δ / (1 - δ))

    open(filename, "a") do f
        println(f, "\n; Output constraints")
        println(f, "(assert (or")

        for i in 0:n_out-1
            # i is max output of NN1 with confidence gap δ
            for k in 0:n_out-1
                if k == i
                    continue
                end

                println(f, "\t(and")
                for j in 0:n_out-1
                    if j != i
                        # yᵢ - yⱼ ≥ T ↔ yᵢ ≥ yⱼ + T
                        println(f, "\t\t(>= (- Y_", i, " Y_", j, ") ", T, ")")
                    end
                end

                println(f, "\t\t(>= Y_", k+n_out, " Y_", i+n_out, ")")
                # close the "(and"
                println(f, "\t)")
            end
        end

        # close the "(assert (or"
        println(f, "))")
    end
end


# same as above but assuming that the product network has n² outputs for the reference network
function write_δ_top1_constraint_n2_out(n_out::Integer, δ::Number, filename)
    T = log(δ / (1 - δ))

    open(filename, "a") do f
        println(f, "\n; Output constraints")
        println(f, "(assert (or")

        for i in 0:n_out-1
            # i is max output of NN1 with confidence gap δ
            # in a vector of z = vec([(i, j) for i in 0:n_out-1, j in 0:n_out-1])
            # corresponding to yᵢ - yⱼ, we now that yᵢ - yⱼ is at position z[i + j*n_out] (nets are in ONNX => python indexing)
            for k in 0:n_out-1
                # k ≠ i is max_output of NN2 (would be a counterexample if SAT)
                if k == i
                    continue
                end

                println(f, "\t(and")

                for j in 0:n_out-1
                    if j != i
                        println(f, "\t\t(>= Y_", i + j*n_out, " ",  T, ")")
                    end
                end

                println(f, "\t\t(>= Y_", k+n_out^2, " Y_", i+n_out^2, ")")
                # close the "(and"
                println(f, "\t)")
            end
        end

        # close the "(assert (or"
        println(f, "))")
    end
end



function write_vnnlib_eps(lbs::AbstractVector, ubs::AbstractVector, n_out::Integer, eps::Real, filename)
    write_declare_vars(length(lbs), n_out, filename)
    write_input_constraints(lbs, ubs, filename)
    write_eps_constraint(n_out, eps, filename)
end


function write_vnnlib_top1(lbs::AbstractVector, ubs::AbstractVector, n_out::Integer, filename)
    write_declare_vars(length(lbs), 2*n_out, filename)
    write_input_constraints(lbs, ubs, filename)
    write_top1_constraint(n_out, filename)
end


function write_vnnlib_δ_top1(lbs::AbstractVector, ubs::AbstractVector, n_out::Integer, δ::Number, filename)
    # since product NN is stacked, we need twice the output neurons
    write_declare_vars(length(lbs), 2*n_out, filename)
    write_input_constraints(lbs, ubs, filename)
    write_δ_top1_constraint(n_out, δ, filename)
end


function write_vnnlib_δ_top1_n2_out(lbs::AbstractVector, ubs::AbstractVector, n_out::Integer, δ::Number, filename)
    # since product NN is stacked, we need twice the output neurons
    write_declare_vars(length(lbs), n_out^2 + n_out, filename)
    write_input_constraints(lbs, ubs, filename)
    write_δ_top1_constraint_n2_out(n_out, δ, filename)
end


function get_product_nn_name(onnx_file1, onnx_file2; mode=:eps)
    name1 = basename(onnx_file1)
    name2 = basename(onnx_file2)
    name = string("product_", name1[1:end-5], "_", name2[1:end-5], "_", mode, ".onnx")
    return name
end


"""
Write intances.csv file containing product network, vnnlib-file for the property and timeout.

onnx_files contain the product networks.
equivalence property of the specified type over the i-th input spec.

args:
    onnx_files - list of product NNs
    input_specs - list of vnnlib files (only input spec is important)
    timeout - timeout for solver in seconds
    filename - name of output file
    out_spec_dir - where to store generated vnnlib files

kwargs:
    type - either :eps for epsilon equivalence or :top1 for top1 equivalence
"""
function create_instances_csv(onnx_files, input_specs, timeout, out_dir; mode=:eps, eps=0.05, δs=nothing)
    if out_dir[end] == "/"
        out_dir = out_dir[1:end-1]
    end

    mkpath(out_dir)
    mkpath(out_dir * "/specs")
    
    open(out_dir * "/instances.csv", "w") do f
        for (i, (prod_nn,in_spec)) in enumerate(zip(onnx_files,input_specs))
            if mode == :eps
                spec_name = string("product_", basename(in_spec)[1:end-7], "_eps_", eps, ".vnnlib")
            elseif mode == :top1
                spec_name = string("product_", basename(in_spec)[1:end-7], "_top1.vnnlib")
            elseif mode == :δtop1 || mode == :δtop1n2
                δ = δs[i]
                spec_name = string("product_", basename(in_spec)[1:end-7], "_delta_$(δ)_top1n2.vnnlib")
            else
                throw(ArgumentError("Unkown mode: Expected :eps or :top1 or :δtop1 got $mode"))
            end

            if !isfile(out_dir * "/specs/" * spec_name)
                ast, n_in, n_out = get_ast(in_spec)
                # don't use the n_out here as the vnnlib file may only contain the input spec!
                if contains(lowercase(prod_nn), "mnist")
                    n_out = 10
                elseif contains(lowercase(prod_nn), "acas")
                    n_out = 5
                else
                    println("Assume you are generating properties for LHC networks!")
                    n_out = 5
                    #throw(ArgumentError("Expected filenames to contain either mnist or acas!"))
                end

                next = iterate(ast)
                (bounds, _, _, _), _ = next
                lbs = bounds[:,1]
                ubs = bounds[:,2]

                if mode == :eps
                    write_vnnlib_eps(lbs, ubs, n_out, eps, out_dir * "/specs/" * spec_name)
                elseif mode == :top1
                    write_vnnlib_top1(lbs, ubs, n_out, out_dir * "/specs/" * spec_name)
                elseif mode == :δtop1
                    δ = δs[i]
                    write_vnnlib_δ_top1(lbs, ubs, n_out, δ, out_dir * "/specs/" * spec_name)
                elseif mode == :δtop1n2
                    δ = δs[i]
                    write_vnnlib_δ_top1_n2_out(lbs, ubs, n_out, δ, out_dir * "/specs/" * spec_name)
                else
                    throw(ArgumentError("Unkown mode: Expected :eps or :top1 or :δtop1, got $mode"))
                end
            end

            println(f, "nets/", basename(prod_nn), ", specs/", spec_name, ", ", timeout)
        end
    end
    
end
