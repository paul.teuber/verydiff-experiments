
import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np

import torchinfo

import onnx
from onnx2pytorch import ConvertModel
from onnxruntime import InferenceSession

from copy import deepcopy
import os

import sys
import argparse


class ProductNNEps(nn.Module):

  def __init__(self, model1, model2):
    super(ProductNNEps, self).__init__()
    self.model1 = model1
    self.model2 = model2

  def forward(self, x):
    y1 = self.model1(x)
    y2 = self.model2(x)
    return y1 - y2
  

class ProductNNTop1(nn.Module):

  def __init__(self, model1, model2):
    super(ProductNNTop1, self).__init__()
    self.model1 = model1
    self.model2 = model2

  def forward(self, x):
    y1 = self.model1(x)
    y2 = self.model2(x)
    # stack along last dimension (first dims can be batch, channel, ...)
    return torch.cat((y1, y2), dim=-1)


class ProductNNTop1N2(nn.Module):

  def __init__(self, model1, model2, n_out):
    super(ProductNNTop1N2, self).__init__()
    self.model1 = model1
    self.model2 = model2
    self.fc = nn.Linear(n_out, n_out**2)
    
  def forward(self, x):
    y1 = self.model1(x)
    y1 = self.fc(y1)
    y2 = self.model2(x)
    return torch.cat((y1, y2), dim=-1)
    



def load_paulsen_model(onnx_path, printing=False):
  """
  MNIST NNs by Paulsen have transposed weight matrices.
  """
  # experimental=True to allow for >1 batch size
  onnx_model = onnx.load(onnx_path)
  model = ConvertModel(onnx_model, experimental=True)

  # they don't all have the dummy layer at the end, so do this manually
  if "3" in onnx_path and "100" in onnx_path:
    model_pt = nn.Sequential(nn.Linear(784, 100), nn.ReLU(), nn.Linear(100, 100), nn.ReLU(), nn.Linear(100, 10), nn.ReLU(), nn.Linear(10, 10))
  elif "2" in onnx_path and "512" in onnx_path:
    model_pt = nn.Sequential(nn.Linear(784, 512), nn.ReLU(), nn.Linear(512, 512), nn.ReLU(), nn.Linear(512, 10))
  elif "4" in onnx_path and "1024" in onnx_path:
    model_pt = nn.Sequential(nn.Linear(784, 1024), nn.ReLU(), nn.Linear(1024, 1024), nn.ReLU(), nn.Linear(1024, 1024), nn.ReLU(), nn.Linear(1024, 10), nn.ReLU(), nn.Linear(10, 10))
  else:
    raise ValueError(f"model {onnx_path} not known!")

  with torch.no_grad():
    for m1, m2 in zip(model.children(), model_pt.children()):
      if isinstance(m1, nn.Linear):
        if printing:
          print("model.w: ", m1.weight.shape)
          print("torch.w: ", m2.weight.shape)

        m2.weight = nn.Parameter(m1.weight)
        m2.bias = nn.Parameter(m1.bias)

  return model_pt


def load_model(onnx_path):
  paulsen_models = ["mnist_relu_3_100.onnx", "mnist_relu_2_512.onnx", "mnist_relu_4_1024.onnx"]
  name = os.path.basename(onnx_path)
  if name in paulsen_models:
    return load_paulsen_model(onnx_path)
  else:
    onnx_model = onnx.load(onnx_path)
    model = ConvertModel(onnx_model, experimental=True)
  return model


def remove_softmax_layer(model):
  assert type(list(model.children())[-1]) == nn.Softmax, "Last layer is not softmax!"
  return torch.nn.Sequential(*list(model.children())[:-1])


def numpy_softmax(x):
  return (np.exp(x - np.max(x))) / np.exp(x - np.max(x)).sum()


def make_product_model(onnx_path1, onnx_path2, out_prefix="", mode="eps"):
  """
  Generates product network of two NNs.
  If mode is "eps", then the output is NN_1(x) - NN_2(x).
  If mode is "top1", then the output is [NN_1(x); NN_2(x)] (stacked along the last dimension)

  Hard-coded to work for the networks in this project:
    - if one of the models by paulsen is used, the weight matrices are transposed
    - sample input is chosen based on presence of "acas" or "mnist" in the filename

  The product network is saved as <out_prefix>/product_<name1>_<name2>.onnx

  kwargs:
    mode - either "eps" for epsilon equivalence, "top1" for top1 equivalence or 
           "top1n2" for delta-top1 equivalence with n**2 + n outputs (s.t. we can use abCROWN)
  """
  model1 = load_model(onnx_path1)
  model2 = load_model(onnx_path2)
  model1_onnx = InferenceSession(onnx_path1)
  model2_onnx = InferenceSession(onnx_path2)
  #print(model1._modules["0"].weight.data.shape)
  #print(model2._modules["MatMul_H0"].weight.data.shape)

  name1 = os.path.basename(onnx_path1)
  name2 = os.path.basename(onnx_path2)
  name = f"{out_prefix}/product_{name1[:-5]}_{name2[:-5]}_{mode}.onnx"

  LHC_FLAG = False
  if "mnist" in name1.lower() or "mnist" in name2.lower():
    x = torch.zeros(1,784)
  elif "acas" in name1.lower() or "acas" in name2.lower():
    x = torch.zeros(1,5)
  else:
    #raise ValueError(f"expected the network name to contain either \"acas\" or \"mnist\", but found: {name1}, {name2}")
    print("assume you want to convert an LHC model!")
    LHC_FLAG = True
    x = torch.zeros(1,16)

  if LHC_FLAG:
    model1 = remove_softmax_layer(model1)
    model2 = remove_softmax_layer(model2)

  if mode == "eps":
    prod_model = ProductNNEps(model1, model2)
  elif mode == "top1n2":
    assert LHC_FLAG, "mode top1n2 only valid for LHC models right now!"
    n_out = 5
    prod_model = ProductNNTop1N2(model1, model2, n_out) 
    
    with torch.no_grad():
      I = torch.eye(n_out)
      W = I.repeat(n_out, 1) - I.repeat_interleave(n_out, dim=0)
      prod_model.fc.weight = nn.Parameter(W)
      prod_model.fc.bias   = nn.Parameter(torch.zeros_like(prod_model.fc.bias))
      
  else:
    prod_model = ProductNNTop1(model1, model2)

  print(prod_model)

  torch.onnx.export(prod_model, x, name, export_params=True, input_names=["X"], output_names=["y_out"])

  prod_onnx = InferenceSession(name)
  print("TESTING PRODUCT NETWORK: ",end="")
  for i in range(1000):
    x = torch.rand(x.shape)

    if LHC_FLAG:
      y1 = model1_onnx.run(None, {"X": x.numpy()})[0]
      y2 = model2_onnx.run(None, {"X": x.numpy()})[0]
      y_prod = prod_onnx.run(None, {"X": x.numpy()})[0]
      
      print("arrived")
      if mode == "top1n2":
        # if we use the onnx models it gets annoying with the softmax
        y1 = model1(x)
        y2 = model2(x)
        y_prod = prod_model(x)
        
        # test 100 times
        for _ in range(100):
          i = np.random.randint(5)
          j = np.random.randint(5)
          k = np.random.randint(5)
          #print(prod_model.fc.weight)
          #print(y1)
          #print(y_prod)
          assert y_prod[0][i + j*5] == y1[0][i] - y1[0][j], f"product network failed (i = {i}, j = {j}): y1[{i}] = {y1[0][i]}, y1[{j}] = {y1[0][j]}, y_prod[{i + j*5}] = {y_prod[0][i + j*5]}"
          assert y_prod[0][5**2 + k] == y2[0][k], f"product network failed (k = {k})"
         
      else:
        y_prod_softmax = np.concatenate((numpy_softmax(y_prod[0][:len(y1[0])]), numpy_softmax(y_prod[0][len(y1[0]):])))
        print(np.concatenate((y1[0], y2[0])))
        print(y_prod_softmax)
        assert np.allclose(np.concatenate((y1[0], y2[0])), y_prod_softmax), "product network failed"
    else:
      x_simple = x[0]
      y1 = model1_onnx.run(None, {"X": x_simple.numpy()})[0]
      if "pruned" in os.path.basename(onnx_path2):
        y2 = model2_onnx.run(None, {"X": x_simple.numpy()})[0]
      else:
        y2 = model2_onnx.run(None, {"X": x.numpy()})[0]
      y_prod = prod_onnx.run(None, {"X": x.numpy()})[0]
      assert np.allclose(y1 - y2, y_prod, atol=1e-3), "product network failed"
  print("PASSED")


if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument("onnx_file1", help="Path to onnx file of reference network")
  parser.add_argument("onnx_file2", help="Path to onnx file of test network")
  parser.add_argument("-o", "--outdir", help="output directory of product network (defaults to \"\")", default="")
  parser.add_argument("-m", "--mode", help="equivalence property: either eps, top1 or top1n2 (defaults to eps)", default="eps")

  args = parser.parse_args()

  onnx_file1 = args.onnx_file1
  onnx_file2 = args.onnx_file2
  out_prefix = args.outdir
  mode = args.mode

  make_product_model(onnx_file1, onnx_file2, out_prefix=out_prefix, mode=mode)
