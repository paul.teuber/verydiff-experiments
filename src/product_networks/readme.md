
Right now, only epsilon equivalence is supported! (An encoding for top-1-equivalence is also implemented in `generate_product_network_spec.jl`, but the VNNLIB parser of $\alpha$-$\beta$-CROWN only supportes DNF specifications, which would be a huge blowup.)

# Creating Product Networks

```shell
python generate_product_networks.py <path/to/reference_nn> <path/to/test_nn> -o <output/path> -m eps
```

Make sure that the necessary packages (see `generate_product_networks.py`) are available in your python environment.


# Creating Specifications

Running

```julia
create_instances_csv(netlist1, netlist2, 300, output_dir)
```

(from `generate_product_network_spec.jl`) will create a directory at `output_dir` with structure
```
output_dir
|- instances.csv
|- vnnlib
|--|-- prop1.vnnlib
|--|-- prop2.vnnlib
|--|-- ...
|- onnx
```

Property files and `instances.csv` are generated automatically, but currently the product networks have to be generated separately as shown above and need to be copied into the `onnx` subdirectory.


# Running $\alpha$-$\beta$-CROWN

Navigate to the directory containing `abcrown.py`, make sure that the configuration file includes the relative path from this location to the location of the `instances.csv` file of the benchmark to test.

```shell
python abcrown.py --config <path/to/config>
```

Make sure to use a python environment where all dependencies of $\alpha$-$\beta$-CROWN are available.

Although example configuration files from past VNNComps are available for MNIST and ACAS networks, those are taylored to GPU or large CPU environments. I haven't yet found a suitable configuration for our setting.