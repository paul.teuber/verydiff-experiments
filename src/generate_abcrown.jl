include("product_networks/generate_product_network_spec.jl")
function generate_benchmarks(cur_dir, benchmark_dir, out_dir, bench_name, all_pruning_factors)
    cur_benchmark_dir = "$(benchmark_dir)/$(bench_name)/nets"
    println("Searching in $(cur_benchmark_dir)")
    cur_out_dir = "$(out_dir)/$(bench_name)/nets"
    mkpath(cur_out_dir)
    # Iterate over all onnx files in the directory
    onnx_files = filter(x->endswith(x,".onnx"), readdir(cur_benchmark_dir,join=true))
    println("Found $(length(onnx_files)) networks")
    for cur_net in onnx_files
        for pruning_factor in all_pruning_factors
            mirror_net = replace(cur_net, "/nets/" => "/nets_pruned/", ".onnx" => "$(pruning_factor).onnx")
            if isfile(mirror_net)
                if !isfile("$(cur_out_dir)/product_$(replace(basename(cur_net),".onnx"=>""))_$(replace(basename(mirror_net),".onnx"=>""))_eps.onnx")
                    cmd = `python $(cur_dir)/product_networks/generate_product_networks.py $(cur_net) $(mirror_net) -o $(cur_out_dir) -m eps`
                    println(cmd)
                    println(run(cmd))
                end
            else
                println("Mirror network $(mirror_net) not found")
            end
        end
    end
end

function generate_abcrown_networks()
    cur_dir = @__DIR__

    benchmark_dir = "$(cur_dir)/../benchmarks/"
    out_dir = "$(cur_dir)/../benchmarks_abcrown/"

    println("1) NETWORKS BY VeryPrune")
    
    println("1.1) PRUNED ACAS (Epsilon)")
    bench_name = "acas-prune"
    all_pruning_factors = ["_pruned5","_pruned10","_pruned30"]
    generate_benchmarks(cur_dir, benchmark_dir, out_dir, bench_name, all_pruning_factors)

    


    println("2) PRUNED MNIST by VeryPrune and Us (Epsilon)")
    bench_name = "mnist-prune"
    #all_pruning_factors = ["_pruned5","_0.49-old","_0.77-old","_0.79-old","_0.79_adam_1_stepsize-old"]
    all_pruning_factors = ["_pruned5"]
    generate_benchmarks(cur_dir, benchmark_dir, out_dir, bench_name, all_pruning_factors)
    
end

function generate_benchmark_specifications(benchmark_dir, out_dir, bench_name; filter_fn=nothing, eps=0.05)
    cur_benchmark_dir = "$(benchmark_dir)/$(bench_name)/specs"
    println("Searching in $(cur_benchmark_dir)")
    cur_net_dir = "$(out_dir)/$(bench_name)/nets"
    cur_out_dir = "$(out_dir)/$(bench_name)/specs"
    mkpath(cur_out_dir)
    all_onnx_files = []
    all_spec_files = []
    onnx_files = filter(x->endswith(x,".onnx"), readdir(cur_net_dir,join=true))
    for cur_net in onnx_files
        for spec_file in filter(x->endswith(x,".vnnlib"), readdir(cur_benchmark_dir,join=true))
            if !isnothing(filter_fn)
                if !filter_fn(basename(spec_file), basename(cur_net))
                    continue
                end
            end
            push!(all_onnx_files, cur_net)
            push!(all_spec_files, spec_file)
        end
    end
    create_instances_csv(all_onnx_files, all_spec_files, 120, "$(out_dir)/$(bench_name)",eps=eps)
end

function acas_network_spec_compatibility(spec_name, network_name)
    #println("Network name $network_name")
    network_name_parts = split(split(network_name,".")[1], "_")
    x = parse(Int, network_name_parts[4])
    y = parse(Int, network_name_parts[5])
    prop_num = parse(Int, split(split(spec_name,".")[1], "_")[2])
    if prop_num == 1
        return true
    elseif prop_num == 2
        return x >= 2
    elseif prop_num == 3
        return !(x == 1 && (y == 7 || y == 8 || y == 9))
    elseif prop_num == 4
        return !(x == 1 && (y == 7 || y == 8 || y == 9))
    elseif prop_num == 5
        return x == 1 && y == 1
    elseif prop_num == 16
        return x == 1 && y == 1
    elseif prop_num == 26
        return x == 1 && y == 1
    elseif prop_num == 7
        return x == 1 && y == 9
    elseif prop_num == 8
        return x == 2 && y == 9
    elseif prop_num == 9
        return x == 3 && y == 3
    elseif prop_num == 10
        return x == 4 && y == 5
    elseif prop_num == 11
        return x == 1 && y == 1
    elseif prop_num == 12
        return x == 3 && y == 3
    elseif prop_num == 13
        return x == 1 && y == 1
    elseif prop_num == 14 || prop_num == 15
        return (x == 4 && y == 1) || (x == 5 && y == 1)
    else
        return false
    end
end

function generate_abcrown_specifications()
    cur_dir = @__DIR__

    benchmark_dir = "$(cur_dir)/../benchmarks/"
    out_dir = "$(cur_dir)/../benchmarks_abcrown/"

    println("1) SPECIFICATIONS BY VeryPrune")
    
    println("1.1) PRUNED ACAS (Epsilon)")
    bench_name = "acas-prune"
    generate_benchmark_specifications(benchmark_dir, out_dir, bench_name,filter_fn=acas_network_spec_compatibility,eps=0.05)

    


    println("2) PRUNED MNIST by VeryPrune and Us (Epsilon)")
    bench_name = "mnist-prune"
    generate_benchmark_specifications(benchmark_dir, out_dir, bench_name,eps=1.0)
end

generate_abcrown_networks()
generate_abcrown_specifications()