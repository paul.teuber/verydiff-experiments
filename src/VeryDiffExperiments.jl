module VeryDiffExperiments

using PyCall
using Pkg.Artifacts

using VeryDiff

include("gurobi_config.jl")

include("acas.jl")
include("mnist.jl")
include("confidence.jl")

nnequiv_verify = nothing
milpequiv_verify = nothing

function __init__()
    pushfirst!(pyimport("sys")."path", artifact"nnequiv"*"/nnequiv-ffc42dbd08a11277dac65b55db731d2ffb66bad9/src")
    #e4eb142857c84e3960742ed127016ebb7b760295
    pushfirst!(pyimport("sys")."path", artifact"milpequiv"*"/NNEquivalence-55e5bc49d39936f2aaa02a9d1326bf36cb0663fb")
    py"""
    import os
    os.environ["OPENBLAS_NUM_THREADS"] = "1"
    os.environ["OMP_NUM_THREADS"] = "1"
    """

    # NNEquiv Function:
py"""
import numpy as np

from nnenum.lp_star import LpStar
from nnenum.onnx_network import load_onnx_network
from nnenum.settings import Settings
from nnenum.zonotope import Zonotope
from nnequiv.equivalence import check_equivalence
from nnequiv.equivalence_properties import EpsilonEquivalence
from nnequiv.equivalence_properties.top1 import Top1Equivalence
from nnequiv.global_state import GLOBAL_STATE

def nnequiv_verify(net1File, net2File, input_bounds, property, to):
    try:
        return nnequiv_verify_internal(net1File, net2File, input_bounds, property, to)
    except Exception as e:
        print("ERROR")
        print(e)
    return None

def nnequiv_verify_internal(net1File, net2File, input_bounds, property, to):
    # Reset global state
    GLOBAL_STATE.RIGHT = 0
    GLOBAL_STATE.WRONG = 0
    GLOBAL_STATE.FINISHED_FRAC = 0
    GLOBAL_STATE.REFINED=0
    GLOBAL_STATE.REFINE_LIMIT=0
    GLOBAL_STATE.REFINE_DEPTH=[]
    GLOBAL_STATE.REFINE_BRANCHING=[]
    Settings.TIMING_STATS = True
    # TODO(steuber): Check out implications of this setting
    Settings.SPLIT_TOLERANCE = 1e-8
    Settings.PARALLEL_ROOT_LP = False
    Settings.NUM_PROCESSES = 1

    strategy = "REFINE_UNTIL_LAST_OPTIMISTIC1"
    Settings.EQUIV_OVERAPPROX_STRAT = strategy
    if strategy.startswith("REFINE_UNTIL"):
        Settings.EQUIV_OVERAPPROX_STRAT_REFINE_UNTIL=True
    print(f"Loading {net1File} and {net2File}")
    network1 = load_onnx_network(net1File)
    network2 = load_onnx_network(net2File)
    if property == "top":
        equivprop = Top1Equivalence()
    else:
        epsilon = float(property)
        equivprop = EpsilonEquivalence(epsilon, networks=[network1, network2])
    inshape = np.prod(network1.get_input_shape())
    print(f"INPUT SHAPE: {inshape}")
    generator = np.identity(inshape, dtype=np.float32)
    non_zero_intervals = input_bounds[:,0] < input_bounds[:,1]
    generator = generator[:,non_zero_intervals]
    bias = np.multiply((np.logical_not(non_zero_intervals)),input_bounds[:,0])
    #np.zeros(inshape, dtype=np.float32)
    bounds = []
    for i in range(len(input_bounds)):
        if non_zero_intervals[i]:
            bounds.append((input_bounds[i][0],input_bounds[i][1]))
    input = Zonotope(bias, generator, init_bounds=bounds)
    check_equivalence(network1, network2, input, equivprop, to=to)
    #print(f"\n[MAIN_TIME] {main_time}")
    print("")
    print("")
"""
    global nnequiv_verify = py"nnequiv_verify"

py"""
from performance import Encoder
import numpy as np
from expression_encoding import flatten, encode_NN_from_file, interval_arithmetic
import time

def check_outputs(nn_file, ins, sort=True, printing=True):
    nn_vars, nn_constraints = encode_NN_from_file(nn_file, ins, ins, '')
    interval_arithmetic(nn_constraints)

    outs = nn_vars[-1]
    if sort:
        outs = sorted(outs, key=lambda x: x.lo)

    if printing:
        for v in outs:
            print(str(v) + ' : ' + str(v.lo))

    return outs

def calculate_violation(ins, path1, path2, top_k=1):
    outs1 = check_outputs(path1, ins, sort=False, printing=False)
    outs2 = check_outputs(path2, ins, sort=False, printing=False)

    a_idx = np.argmax([a.lo for a in outs1])
    b_atop = outs2[a_idx]

    outs1 = sorted(outs1, key=lambda x: -x.lo)
    outs2 = sorted(outs2, key=lambda x: -x.lo)

    b_k = outs2[top_k - 1]

    return b_k.lo - b_atop.lo

def get_grb_inputs(model, numins):
    return [model.getVarByName('i_0_{idx}'.format(idx=j)).X for j in range(numins)]

def calculate_distance(ins, path1, path2, mode='chebyshev'):
    outs1 = check_outputs(path1, ins, sort=False, printing=False)
    outs2 = check_outputs(path2, ins, sort=False, printing=False)
    outs1 = [x.lo for x in outs1]
    outs2 = [x.lo for x in outs2]

    diffs = [abs(x - y) for x, y in zip(outs1, outs2)]

    if mode == 'chebyshev':
        return max(diffs)
    elif mode == 'manhattan':
        return sum(diffs)
    else:
        raise ValueError(f"mode {mode} no known!")


def milpequiv_verify(net1File, net2File, input_bounds, property, to, bounds_tightening):
    print(net1File)
    print(net2File)
    print(input_bounds)
    print(property)
    print(to)
    print(bounds_tightening)

    input_los = input_bounds[:,0]
    input_his = input_bounds[:,1]
    dim = len(input_los)
    if property == "top":
        mode = "one_hot_partial_top_1"
    elif isinstance(property, float):
        mode = "optimize_diff_chebyshev"
    else:
        raise ValueError(f"Unknown property {property}")
    reference = net1File
    test = net2File
    enc = Encoder()
    enc.encode_equiv(reference, test, input_los, input_his, mode)

    start_time = time.time()
    if bounds_tightening:
        enc.set_total_timeout(to)
        try:
            enc.optimize_constraints()
        except TimeoutError:
            print("TIMEOUT")
            return
        to = int(np.floor(enc.total_timeout))

    
    model = enc.create_gurobi_model()

    model.setParam('TimeLimit', to)
    model.setParam('Threads', 1)
    # TODO(steuber): With or without this?
    #model.setParam('BestObjStop', 20.)
    if isinstance(property, float):
        model.setParam('BestObjStop', property)
        model.setParam('BestBdStop', property)

    model.optimize()

    print("")
    print("")
    if model.status == 9: # TIME_LIMIT
        print("TIMEOUT")
        return
    elif model.status == 3: # INFEASIBLE
        print("ERROR: Gurobi Model is not feasible")
        return
    else:
        try:
            ins = get_grb_inputs(model, dim)
        except:
            print("Error during solution retrieval with status ", model.status)
            if isinstance(property, float) and model.getAttr("ObjBound") <= property:
                print("NETWORKS EQUIVALENT")
                return
            if not isinstance(property, float) and model.getAttr("ObjBound") <= 0.0:
                print("NETWORKS EQUIVALENT")
                return
            print("UNKNOWN")
            return
        if isinstance(property, float):
            if calculate_distance(ins, reference, test, mode='chebyshev') > property:
                print("NETWORKS NOT EQUIVALENT")
                outs1 = check_outputs(reference, ins, sort=False, printing=True)
                outs2 = check_outputs(test, ins, sort=False, printing=True)
                print("Distance: ", calculate_distance(ins, reference, test, mode='chebyshev'))
                print("Input: ", ins)
            elif model.getAttr("ObjBound") <= property:
                print("NETWORKS EQUIVALENT")
            else:
                print("UNKNOWN")
        if not isinstance(property, float):
            if calculate_violation(ins, reference, test, top_k=1)>0.0:
                print("NETWORKS NOT EQUIVALENT")
                print("Distance: ", calculate_violation(ins, reference, test, top_k=1))
                print("Input: ", ins)
            elif model.getAttr("ObjBound") <= 0.0:
                print("NETWORKS EQUIVALENT")
            else:
                print("UNKNOWN")

"""
global milpequiv_verify = py"milpequiv_verify"
end

include("warmup.jl")
include("run.jl")
include("run-all.jl")
#include("run-all-pool5.jl")
include("run-now.jl")
#include("run-quant.jl")

export run_experiments, run_experiments_updated

end # module VeryDiffExperiments
