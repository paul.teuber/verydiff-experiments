include("product_networks/generate_product_network_spec.jl")

function generate_abcrown_networks()
    cur_dir = @__DIR__
    out_dir = "$(cur_dir)/../benchmarks_abcrown/lhc"
    net_out_dir = "$(out_dir)/nets"
    mkpath(net_out_dir)

    csv_file = "$(cur_dir)/../analysis-new/ab_todo.csv"
    lines = readlines(csv_file)

    for line in lines[2:end]
        net, mirror, specs, δ, timeout = split(line, ",")
        if !isfile("$(net_out_dir)/product_$(replace(basename(net), ".onnx" => ""))_$(replace(basename(mirror), ".onnx" => ""))_top1.onnx")
            # cmd = `conda run -n convertOnnx python $(cur_dir)/product_networks/generate_product_networks.py $(net) $(mirror) -o $(net_out_dir) -m top1`
            cmd = `conda run -n convertOnnx python $(cur_dir)/product_networks/generate_product_networks.py $(net) $(mirror) -o $(net_out_dir) -m top1n2`
            println(cmd)
            run(cmd)
        end
    end
end


function generate_abcrown_specifications(;mode=:δtop1)
    cur_dir = @__DIR__
    out_dir = "$(cur_dir)/../benchmarks_abcrown/lhc"
    net_out_dir = "$(out_dir)/nets"
    spec_out_dir = "$(out_dir)/specs"
    mkpath(net_out_dir)

    csv_file = "$(cur_dir)/../analysis-new/ab_todo.csv"
    lines = readlines(csv_file)

    onnx_files = []
    input_specs = []
    timeouts = []
    δs = []
    for line in lines[2:end]
        net, mirror, specs, δ, timeout = split(line, ",")
        prod_net_name = "$(net_out_dir)/product_$(replace(basename(net), ".onnx" => ""))_$(replace(basename(mirror), ".onnx" => ""))_$(mode).onnx"
        push!(onnx_files, prod_net_name)
        push!(input_specs, specs)
        push!(δs, parse(Float64, δ))
        push!(timeouts, timeout)
    end

    # we can only give a scalar for timeout right now - but doesn't matter since they are all 600 anyways
    # TODO: make VNNLib.get_ast(s::String) to VNNLib.get_ast(s::AbstractString) or it won't accept SubString!!!
    create_instances_csv(onnx_files, String.(input_specs), timeouts[1], out_dir, mode=mode, δs=δs)
end


generate_abcrown_networks()
#generate_abcrown_specifications()
generate_abcrown_specifications(mode=:δtop1n2)
