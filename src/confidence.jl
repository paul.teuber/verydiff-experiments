function verify_top1(network_file1, network_file2, vnnlib_file; timeout=120, delta=zero(Float64),naive=false)
    println("Parsing $(network_file1)...")
    network1 = parse_network(VNNLib.load_network(network_file1))
    println("Parsing $(network_file2)...")
    network2 = parse_network(VNNLib.load_network(network_file2))
    f, n_inputs, _ = get_ast(vnnlib_file)

    for (bounds, _, _, num) in f
        property_check = get_top1_property(naive=naive, delta=delta) #scale=1.05)
        split_heuristic = top1_configure_split_heuristic(1)
        verify_network(network1, network2, bounds[1:n_inputs,:], property_check, split_heuristic;timeout=timeout)
    end
end

function verify_eps(epsilon, network_file1, network_file2, vnnlib_file; timeout=120,naive=false)
    println("Parsing $(network_file1)...")
    network1 = parse_network(VNNLib.load_network(network_file1))
    println("Parsing $(network_file2)...")
    network2 = parse_network(VNNLib.load_network(network_file2))
    f, n_inputs, _ = get_ast(vnnlib_file)

    for (bounds, _, _, num) in f
        property_check = if naive
            get_epsilon_property_naive(epsilon)
        else
            get_epsilon_property(epsilon)
        end
        split_heuristic = epsilon_split_heuristic
        verify_network(network1, network2, bounds[1:n_inputs,:], property_check, split_heuristic;timeout=timeout)
    end
end

function verify_rice(;folder="rice-1", pruning_factors=["0.1","0.2","0.3","0.4","0.5"])
    verify_top1("new_benchmarks/$(folder)/nets/2_40.onnx", "new_benchmarks/$(folder)/nets_pruned/2_40-0.1.onnx", "new_benchmarks/$(folder)/specs/prop_1.vnnlib";delta=0.5,timeout=600,naive=true)
    for naive in [false, true]
        VeryDiff.USE_DIFFZONO=!naive
        experiment_path_prefix = "experiments_final/$(folder)/"
        if naive
            experiment_path_prefix *= "VeryDiff-Naive/"
        else
            experiment_path_prefix *= "VeryDiff-Combined/"
        end
        mkpath(experiment_path_prefix)
        for orig_net in ["2_20","2_40","2_80","4_40"]
            for pruning in pruning_factors
                log_name = "$(orig_net)-$(pruning)"
                prefix = "new_benchmarks/$(folder)"
                mirror_net_file = "$(prefix)/nets_pruned/$(orig_net)-$(pruning).onnx"
                orig_net_file = "$(prefix)/nets/$(orig_net).onnx"
                spec_file = "$(prefix)/specs/prop_1.vnnlib"
                for delta in [0.5, 0.9, 1-1e-1, 1-1e-2, 1-1e-3, 1-1e-4, 1-1e-5, 1-1e-6, 1-1e-7]
                    current_experiment_path = "$(experiment_path_prefix)/$(log_name)-$(delta).log"
                    original_stdout = stdout
                    original_stderr = stderr
                    try
                        if isfile(current_experiment_path)
                            println("Skipping $(current_experiment_path)")
                            continue
                        end
                        open(current_experiment_path, "w") do f
                            redirect_stdout(f)
                            redirect_stderr(f)
                            cur_runtime = @elapsed verify_top1(orig_net_file, mirror_net_file, spec_file; delta=delta, naive=naive, timeout=600)
                            println("[VERYDIFF_TIMER] $cur_runtime")
                        end
                    finally
                        redirect_stdout(original_stdout)
                        redirect_stderr(original_stderr)
                    end
                end
            end
        end
    end
end

function verify_rice_sigma(;folder="rice-1", pruning_factors=["0.1","0.2","0.3","0.4","0.5"])
    verify_top1("new_benchmarks/$(folder)/nets/2_40.onnx", "new_benchmarks/$(folder)/nets_pruned/2_40-0.1.onnx", "new_benchmarks/$(folder)/specs/prop_1.vnnlib";delta=0.5,timeout=600,naive=true)
    for naive in [false, true]
        VeryDiff.USE_DIFFZONO=!naive
        experiment_path_prefix = "experiments_final/$(folder)-sigma/"
        if naive
            experiment_path_prefix *= "VeryDiff-Naive/"
        else
            experiment_path_prefix *= "VeryDiff-Combined/"
        end
        mkpath(experiment_path_prefix)
        for orig_net in ["2_20","2_40","2_80","4_40"]
            for pruning in pruning_factors
                log_name = "$(orig_net)-$(pruning)"
                prefix = "new_benchmarks/$(folder)"
                mirror_net_file = "$(prefix)/nets_pruned/$(orig_net)-$(pruning).onnx"
                orig_net_file = "$(prefix)/nets/$(orig_net).onnx"
                for delta in [0.5, 0.9, 1-1e-1, 1-1e-2, 1-1e-3, 1-1e-4, 1-1e-5, 1-1e-6, 1-1e-7]
                    current_experiment_path = "$(experiment_path_prefix)/$(log_name)-$(delta).log"
                    original_stdout = stdout
                    original_stderr = stderr
                    try
                        if isfile(current_experiment_path)
                            println("Skipping $(current_experiment_path)")
                            continue
                        end
                        open(current_experiment_path, "w") do f
                            redirect_stdout(f)
                            redirect_stderr(f)
                            for sigma in [0.5,1.0,2.0,3.0]
                                println("[VERYDIFF_EXPERIMENT] Sigma: $sigma")
                                spec_file = "$(prefix)/specs/sigma_$sigma.vnnlib"
                                cur_runtime = @elapsed verify_top1(orig_net_file, mirror_net_file, spec_file; delta=delta, naive=naive, timeout=600)
                                println("[VERYDIFF_TIMER] $cur_runtime")
                            end
                        end
                    finally
                        redirect_stdout(original_stdout)
                        redirect_stderr(original_stderr)
                    end
                end
            end
        end
    end
end

function verify_lhc_sigma(;folder="lhc", pruning_factors=["0.1","0.2","0.3","0.4","0.5"])
    verify_top1("new_benchmarks/$(folder)/nets/2_20-0.1.onnx", "new_benchmarks/$(folder)/nets_pruned/2_20-0.1-0.1.onnx", "new_benchmarks/$(folder)/specs/sigma_0.1.vnnlib";delta=0.5,timeout=600,naive=true)
    
    for orig_net in ["2_20-0.1","2_20-1","2_40-0.1","2_40-1","2_80-0.1","2_80-1","4_20-0.1","4_20-1"]
        for pruning in pruning_factors
            log_name = "$(orig_net)-$(pruning)"
            prefix = "new_benchmarks/$(folder)"
            mirror_net_file = "$(prefix)/nets_pruned/$(orig_net)-$(pruning).onnx"
            orig_net_file = "$(prefix)/nets/$(orig_net).onnx"
            for delta in [0.5, 0.9, 1-1e-1, 1-1e-2, 1-1e-3, 1-1e-4, 1-1e-5, 1-1e-6, 1-1e-7]
                for naive in [false, true]
                    VeryDiff.USE_DIFFZONO=!naive
                    experiment_path_prefix = "experiments_final/$(folder)-sigma/"
                    if naive
                        experiment_path_prefix *= "VeryDiff-Naive/"
                    else
                        experiment_path_prefix *= "VeryDiff-Combined/"
                    end
                    mkpath(experiment_path_prefix)
                    current_experiment_path = "$(experiment_path_prefix)/$(log_name)-$(delta).log"
                    original_stdout = stdout
                    original_stderr = stderr
                    try
                        if isfile(current_experiment_path)
                            println("Skipping $(current_experiment_path)")
                            continue
                        end
                        open(current_experiment_path, "w") do f
                            redirect_stdout(f)
                            redirect_stderr(f)
                            for sigma in [0.1,0.5,1.0,2.0,3.0]
                                println("[VERYDIFF_EXPERIMENT] Sigma: $sigma")
                                spec_file = "$(prefix)/specs/sigma_$sigma.vnnlib"
                                cur_runtime = @elapsed verify_top1(orig_net_file, mirror_net_file, spec_file; delta=delta, naive=naive, timeout=600)
                                println("[VERYDIFF_TIMER] $cur_runtime")
                            end
                        end
                    finally
                        redirect_stdout(original_stdout)
                        redirect_stderr(original_stderr)
                    end
                end
            end
        end
    end
end


# function verify_lhc_sigma_eps(epsilon;folder="lhc", pruning_factors=["0.1","0.2","0.3","0.4","0.5"])
#     verify_eps(epsilon, "new_benchmarks/$(folder)/nets/lhc_4_20.onnx", "new_benchmarks/$(folder)/nets_pruned/lhc_4_20-pruned-0.1.onnx", "new_benchmarks/$(folder)/specs/sigma_0.5.vnnlib";timeout=600,naive=false)
#     for naive in [false, true]
#         VeryDiff.USE_DIFFZONO=!naive
#         experiment_path_prefix = "experiments_final/$(folder)-sigma-$(epsilon)/"
#         if naive
#             experiment_path_prefix *= "VeryDiff-Naive/"
#         else
#             experiment_path_prefix *= "VeryDiff-Combined/"
#         end
#         mkpath(experiment_path_prefix)
#         for orig_net in ["lhc_4_20"] #,"lhc_2_40"]
#             for pruning in pruning_factors
#                 log_name = "$(orig_net)-$(pruning)"
#                 prefix = "new_benchmarks/$(folder)"
#                 mirror_net_file = "$(prefix)/nets_pruned/$(orig_net)-pruned-$(pruning).onnx"
#                 orig_net_file = "$(prefix)/nets/$(orig_net).onnx"
#                     current_experiment_path = "$(experiment_path_prefix)/$(log_name).log"
#                     original_stdout = stdout
#                     original_stderr = stderr
#                     try
#                         if isfile(current_experiment_path)
#                             println("Skipping $(current_experiment_path)")
#                             continue
#                         end
#                         open(current_experiment_path, "w") do f
#                             redirect_stdout(f)
#                             redirect_stderr(f)
#                             for sigma in [0.1,0.5,1.0,2.0,3.0]
#                                 println("[VERYDIFF_EXPERIMENT] Sigma: $sigma")
#                                 spec_file = "$(prefix)/specs/sigma_$sigma.vnnlib"
#                                 cur_runtime = @elapsed verify_eps(epsilon, orig_net_file, mirror_net_file, spec_file; naive=naive, timeout=600)
#                                 println("[VERYDIFF_TIMER] $cur_runtime")
#                             end
#                         end
#                     finally
#                         redirect_stdout(original_stdout)
#                         redirect_stderr(original_stderr)
#                     end
#                 end
#         end
#     end
# end