using VNNLib

CONTROL_FOCUS = [0, 0, 0, 0, 4, -1, 4, 1, 3, 0, 4, 0, 0, 4, 3, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1, 0]

function acas_network_truncated_inner(network, bounds, epsilon;focus_dim=focus_dim)
    N32 = VeryDiff.truncate_network(Float32,network)
    N16 = VeryDiff.truncate_network(Float16,network)

    property_check = get_epsilon_property(epsilon;focus_dim=focus_dim)
    split_heuristic = epsilon_split_heuristic
    verify_network(N16,N32,bounds, property_check, split_heuristic)
end

function acas_network_truncated(network_file, vnnlib_file, epsilon;focus=false)
    network = parse_network(VNNLib.load_network(network_file))
    f, n_inputs, _ = get_ast(vnnlib_file)
    focus_dim = nothing
    if focus
        prop = basename(vnnlib_file)
        control_num = parse(Int,split(split(prop,".")[1],"_")[2])
        focus_dim = CONTROL_FOCUS[control_num]+1
    end

    for (bounds, _, _, num) in f
        acas_network_truncated_inner(network, bounds[1:n_inputs,:], epsilon;focus_dim=focus_dim)
    end
end

function make_acas_verifier_pruned(pruning;naive=false,quantitative=false)
    @assert pruning in [5,10,30]
    return (network_file, vnnlib_file, epsilon; focus=false) -> begin
        network1 = parse_network(VNNLib.load_network(network_file))
        network_file2 = replace(network_file, ".onnx" => "_pruned$pruning.onnx", "/nets/" => "/nets_pruned/")
        network2 = parse_network(VNNLib.load_network(network_file2))
        f, n_inputs, _ = get_ast(vnnlib_file)
        focus_dim = nothing
        if focus
            prop = basename(vnnlib_file)
            control_num = parse(Int,split(split(prop,".")[1],"_")[2])
            focus_dim = CONTROL_FOCUS[control_num]+1
        end

        for (bounds, _, _, num) in f
            property_check = if naive
                get_epsilon_property_naive(epsilon;focus_dim=focus_dim)
            else
                get_epsilon_property(epsilon;focus_dim=focus_dim)
            end
            split_heuristic = epsilon_split_heuristic
            verify_network(network1, network2, bounds[1:n_inputs,:], property_check, split_heuristic;timeout=120)
        end
    end
end

function acas_network_retrained(network_file1, vnnlib_file, epsilon;focus=false)
    network_file2 = joinpath(dirname(network_file1),"../nets-mirror",basename(network_file1))
    network1 = parse_network(VNNLib.load_network(network_file1))
    network2 = parse_network(VNNLib.load_network(network_file2))
    f, n_inputs, _ = get_ast(vnnlib_file)
    focus_dim = nothing
    if focus
        prop = basename(vnnlib_file)
        control_num = parse(Int,split(split(prop,".")[1],"_")[2])
        focus_dim = CONTROL_FOCUS[control_num]+1
    end

    for (bounds, _, _, num) in f
        property_check = get_epsilon_property(epsilon;focus_dim=focus_dim)
        split_heuristic = epsilon_split_heuristic
        verify_network(network1, network2, bounds[1:n_inputs,:], property_check, split_heuristic)
    end
end

function acas_network_truncated_neurodiff(network_file, vnnlib_file, epsilon;focus=false)
    spec_name = basename(vnnlib_file)
    network_name = split(basename(network_file),".")[1]
    prop_num = parse(Int, split(split(spec_name,".")[1], "_")[2])
    path = "../NeuroDiff/DiffNN-Code"
    command = `$path/delta_network_test $prop_num $path/nnet/$network_name.nnet $path/compressed_nnets/$(network_name)_16bit.nnet $epsilon`
    command = setenv(command, ("OPENBLAS_NUM_THREADS" => "1"))
    command = setenv(command, ("OMP_NUM_THREADS" => "1"))
    command = setenv(command, ("LD_LIBRARY_PATH" => ENV["LD_LIBRARY_PATH"]))
    print(command)
    println(run(command))
end

function make_acas_verifier_pruned_neurodiff(pruning)
    @assert pruning in [5,10,30]
    return (network_file, vnnlib_file, epsilon; focus=false) -> begin
        spec_name = basename(vnnlib_file)
        network_name = split(basename(network_file),".")[1]
        prop_num = parse(Int, split(split(spec_name,".")[1], "_")[2])
        network_dir = dirname(network_file)
        network1 = joinpath(network_dir, network_name*".nnet")
        network2 = joinpath(replace(network_dir, "nets" => "nets_pruned"), network_name*"_pruned$pruning.nnet")
        path = "../NeuroDiff/DiffNN-Code"
        command = `timeout 120s $path/delta_network_test $prop_num $network1 $network2 $epsilon`
    command = setenv(command, ("OPENBLAS_NUM_THREADS" => "1"))
    command = setenv(command, ("OMP_NUM_THREADS" => "1"))
    command = setenv(command, ("LD_LIBRARY_PATH" => ENV["LD_LIBRARY_PATH"]))
    print(command)
    res = nothing
    try
        res = run(command)
    catch e
        println("TIMEOUT")
        println(e)
    finally
        println(res)
    end
    end
end

function acas_network_spec_compatibility(spec_name, network_name)
    #println("Network name $network_name")
    network_name_parts = split(split(network_name,".")[1], "_")
    x = parse(Int, network_name_parts[3])
    y = parse(Int, network_name_parts[4])
    prop_num = parse(Int, split(split(spec_name,".")[1], "_")[2])
    if prop_num == 1
        return true
    elseif prop_num == 2
        return x >= 2
    elseif prop_num == 3
        return !(x == 1 && (y == 7 || y == 8 || y == 9))
    elseif prop_num == 4
        return !(x == 1 && (y == 7 || y == 8 || y == 9))
    elseif prop_num == 5
        return x == 1 && y == 1
    elseif prop_num == 16
        return x == 1 && y == 1
    elseif prop_num == 26
        return x == 1 && y == 1
    elseif prop_num == 7
        return x == 1 && y == 9
    elseif prop_num == 8
        return x == 2 && y == 9
    elseif prop_num == 9
        return x == 3 && y == 3
    elseif prop_num == 10
        return x == 4 && y == 5
    elseif prop_num == 11
        return x == 1 && y == 1
    elseif prop_num == 12
        return x == 3 && y == 3
    elseif prop_num == 13
        return x == 1 && y == 1
    elseif prop_num == 14 || prop_num == 15
        return (x == 4 && y == 1) || (x == 5 && y == 1)
    else
        return false
    end
end

function run_acas_all(benchmark_dir, log_dir, run_name, epsilon, run_eval_function;focus=true)
    log_dir = joinpath(log_dir, "acas-$epsilon", run_name)
    mkpath(log_dir)
    original_stdout = stdout
    original_stderr = stderr
    spec_paths = joinpath.(benchmark_dir, "specs", readdir(joinpath(benchmark_dir,"specs")))
    benchmark_paths = joinpath.(benchmark_dir,"nets", readdir(joinpath(benchmark_dir,"nets")))
    total_time = @elapsed for spec_file in spec_paths
        spec_name = basename(spec_file)
        log_file_name = joinpath(log_dir,"$spec_name.log")
        if isfile(log_file_name)
            continue
        else
            open(log_file_name, "w") do f
                redirect_stdout(f)
                redirect_stderr(f)
                spec_time = @elapsed begin
                    for net_file in benchmark_paths
                        if !endswith(net_file, ".onnx")
                            continue
                        end
                        benchmark_name = basename(net_file)
                        if acas_network_spec_compatibility(spec_name, benchmark_name)
                            println("\n[VERYDIFF_EXPERIMENT] Running on $spec_file $net_file")
                            flush(stdout)
                            flush(stderr)
                            #acas_network_truncated
                            cur_runtime = @elapsed run_eval_function(net_file, spec_file, epsilon;focus=focus)
                            flush(stdout)
                            flush(stderr)
                            println("")
                            println("[VERYDIFF_TIMER] $cur_runtime")
                            GC.gc()
                        end
                    end
                end
                println("\n[VERYDIFF_EXPERIMENT] NETWORK: $spec_file")
                println("[VERYDIFF_EXPERIMENT] TIME: $spec_time")
                redirect_stdout(original_stdout)
                redirect_stderr(original_stderr)
            end
        end
    end
    println("\n[ACAS_EXPERIMENT] Total time: $total_time")
end