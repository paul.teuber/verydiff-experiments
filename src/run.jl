function run_experiments()

println("Warmup...")

cur_dir = @__DIR__

acas_network_truncated("$(cur_dir)/../benchmarks/acas/nets/ACASXU_run2a_1_1_batch_2000.onnx","$(cur_dir)/../benchmarks/acas-retrain/specs/prop_1.vnnlib",0.05;focus=true)
acas_network_truncated("$(cur_dir)/../benchmarks/acas/nets/ACASXU_run2a_1_1_batch_2000.onnx","$(cur_dir)/../benchmarks/acas-retrain/specs/prop_1.vnnlib",0.05;focus=false)
acas_network_truncated_neurodiff("$(cur_dir)/../benchmarks/acas/nets/ACASXU_run2a_1_1_batch_2000.onnx","$(cur_dir)/../benchmarks/acas/specs/prop_1.vnnlib",0.05)

mnist_network_truncated_top1("$(cur_dir)/../benchmarks/mnist/nets/mnist_relu_2_512.onnx","$(cur_dir)/../benchmarks/mnist/specs/mnist_0_local_15.vnnlib",0)

(make_mnist_verifier_pruned_top1_nnequiv(5))("$(cur_dir)/../benchmarks/mnist-prune/nets/mnist_relu_2_512.onnx","$(cur_dir)/../benchmarks/mnist-prune/specs/mnist_0_global_3.vnnlib",0)

(make_mnist_verifier_pruned_top1_milpequiv(5))("$(cur_dir)/../benchmarks/mnist-prune/nets/mnist_relu_2_512.onnx","$(cur_dir)/../benchmarks/mnist-prune/specs/mnist_0_global_3.vnnlib",0;timeout=20)

(make_mnist_verifier_pruned_epsilon_nnequiv(5))("$(cur_dir)/../benchmarks/mnist-prune/nets/mnist_relu_2_512.onnx","$(cur_dir)/../benchmarks/mnist-prune/specs/mnist_0_global_3.vnnlib",1.0)

(make_mnist_verifier_pruned_epsilon_milpequiv(5))("$(cur_dir)/../benchmarks/mnist-prune/nets/mnist_relu_2_512.onnx","$(cur_dir)/../benchmarks/mnist-prune/specs/mnist_0_global_3.vnnlib",1.0;timeout=20)

println("Running ACAS experiments...")

# run_acas_all("$(cur_dir)/../benchmarks/acas","$(cur_dir)/../experiments","NeuroDiff-OneThread",0.05, acas_network_truncated_neurodiff;focus=true)

# PRUNED ACAS VERYDIFF

run_acas_all("$(cur_dir)/../benchmarks/acas-prune","$(cur_dir)/../experiments","VeryDiff-OneThread-Focus-5",0.05, make_acas_verifier_pruned(5);focus=true)
run_acas_all("$(cur_dir)/../benchmarks/acas-prune","$(cur_dir)/../experiments","VeryDiff-OneThread-Focus-10",0.05, make_acas_verifier_pruned(10);focus=true)
run_acas_all("$(cur_dir)/../benchmarks/acas-prune","$(cur_dir)/../experiments","VeryDiff-OneThread-Focus-30",0.05, make_acas_verifier_pruned(30);focus=true)

run_acas_all("$(cur_dir)/../benchmarks/acas-prune","$(cur_dir)/../experiments","VeryDiff-OneThread-5",0.05, make_acas_verifier_pruned(5);focus=false)
run_acas_all("$(cur_dir)/../benchmarks/acas-prune","$(cur_dir)/../experiments","VeryDiff-OneThread-10",0.05, make_acas_verifier_pruned(10);focus=false)
run_acas_all("$(cur_dir)/../benchmarks/acas-prune","$(cur_dir)/../experiments","VeryDiff-OneThread-30",0.05, make_acas_verifier_pruned(30);focus=false)

# PRUNED ACAS NEURODIFF

run_acas_all("$(cur_dir)/../benchmarks/acas-prune","$(cur_dir)/../experiments","NeuroDiff-OneThread-5",0.05, make_acas_verifier_pruned_neurodiff(5);focus=true)
run_acas_all("$(cur_dir)/../benchmarks/acas-prune","$(cur_dir)/../experiments","NeuroDiff-OneThread-10",0.05, make_acas_verifier_pruned_neurodiff(10);focus=true)
run_acas_all("$(cur_dir)/../benchmarks/acas-prune","$(cur_dir)/../experiments","NeuroDiff-OneThread-30",0.05, make_acas_verifier_pruned_neurodiff(30);focus=true)

# PRUNED ACAS MILP/NNEQUIV

#run_acas_all("$(cur_dir)/../benchmarks/acas-prune","$(cur_dir)/../experiments","NNEquiv-OneThread-5",0.05, make_mnist_verifier_pruned_epsilon_nnequiv(5); focus=false)
#run_acas_all("$(cur_dir)/../benchmarks/acas-prune","$(cur_dir)/../experiments","MILPEquiv-OneThread-BoundsTightening-5",0.05, make_mnist_verifier_pruned_epsilon_milpequiv(5); focus=false)

println("Running MNIST experiments...")

# PRUNED MNIST (VeryDiff, NNEquiv, MILPEquiv)
run_mnist_all_top1("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments","VeryDiff-OneThread-5",0, make_mnist_verifier_pruned_top1(5); timeout=120)
run_mnist_all_top1("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments","NNEquiv-OneThread-5",0, make_mnist_verifier_pruned_top1_nnequiv(5); timeout=120)
#Only ~25 benchmark images
#run_mnist_all_top1("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments","MILPEquiv-OneThread-5",0, make_mnist_verifier_pruned_top1_milpequiv(5); timeout=120)
# Comparison with/without bounds tightening
run_mnist_all_top1("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments","MILPEquiv-OneThread-BoundsTightening-5",1, make_mnist_verifier_pruned_top1_milpequiv(5); timeout=120)

# Epsilon Equivalence for MNIST
run_mnist_all("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments","VeryDiff-OneThread-Focus-5",1.0, make_mnist_verifier_pruned(5);focus=true)
run_mnist_all("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments","NeuroDiff-OneThread-Focus-5",1.0, make_mnist_verifier_pruned_neurodiff(5);focus=true)

# Full Epsilon Equivalence
run_mnist_all("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments","VeryDiff-OneThread-5",1.0, make_mnist_verifier_pruned(5);focus=false)
run_mnist_all("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments","NNEquiv-OneThread-5",1.0, make_mnist_verifier_pruned_epsilon_nnequiv(5); focus=false)
#run_mnist_all("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments","MILPEquiv-OneThread-BoundsTightening-5",1.0, make_mnist_verifier_pruned_epsilon_milpequiv(5); focus=false)


# TRUNCATED

#run_acas_all("$(cur_dir)/../benchmarks/acas","$(cur_dir)/../experiments","VeryDiff-OneThread",0.05, acas_network_truncated;focus=false)
#run_acas_all("$(cur_dir)/../benchmarks/acas","$(cur_dir)/../experiments","VeryDiff-OneThread-Focus",0.05, acas_network_truncated;focus=true)

#run_mnist_all_top1("$(cur_dir)/../benchmarks/mnist","$(cur_dir)/../experiments","VeryDiff-OneThread",1, mnist_network_truncated_top1; timeout=120)

#run_mnist_all("$(cur_dir)/../benchmarks/mnist","$(cur_dir)/../experiments","VeryDiff-OneThread",1.0, mnist_network_truncated_epsilon;focus=false)
#run_mnist_all("$(cur_dir)/../benchmarks/mnist","$(cur_dir)/../experiments","VeryDiff-OneThread-Focus",1.0, mnist_network_truncated_epsilon;focus=true)
#run_mnist_all("$(cur_dir)/../benchmarks/mnist-large","$(cur_dir)/../experiments","VeryDiff-OneThread-Focus",1.0, mnist_network_truncated_epsilon;focus=true)

#run_mnist_all("$(cur_dir)/../benchmarks/mnist","$(cur_dir)/../experiments","VeryDiff-OneThread",1.0, mnist_network_truncated_epsilon;focus=false)

#run_mnist_all_top1("$(cur_dir)/../benchmarks/mnist","$(cur_dir)/../experiments","VeryDiff-OneThread-0",0, mnist_network_truncated_top1; timeout=120)

# Pruned NNs
# run_mnist_all_top1_new("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments","VeryDiff-OneThread-2_512_0.79",0, make_mnist_verifier_pruned_top1("_0.79"); timeout=120,filter=x->contains(x,"mnist_relu_2_512"))

# run_mnist_all_top1_new("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments","VeryDiff-OneThread-3_100_0.49",0, make_mnist_verifier_pruned_top1("_0.49"); timeout=120,filter=x->contains(x,"mnist_relu_3_100"))

# run_mnist_all_top1_new("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments","VeryDiff-OneThread-3_100_0.77",0, make_mnist_verifier_pruned_top1("_0.77"); timeout=120,filter=x->contains(x,"mnist_relu_3_100"))

# run_mnist_all_top1_new("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments","VeryDiff-OneThread-3_100_0.79",0, make_mnist_verifier_pruned_top1("_0.79_adam_1_stepsize"); timeout=120,filter=x->contains(x,"mnist_relu_3_100"))

# NNEquiv
run_mnist_all_top1_new("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments","NNEquiv-OneThread-2_512_0.79",0, make_mnist_verifier_pruned_top1_nnequiv("_0.79-old"); timeout=120,filter=x->contains(x,"mnist_relu_2_512"))

run_mnist_all_top1_new("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments","NNEquiv-OneThread-3_100_0.49",0, make_mnist_verifier_pruned_top1_nnequiv("_0.49-old"); timeout=120,filter=x->contains(x,"mnist_relu_3_100"))

run_mnist_all_top1_new("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments","NNEquiv-OneThread-3_100_0.77",0, make_mnist_verifier_pruned_top1_nnequiv("_0.77-old"); timeout=120,filter=x->contains(x,"mnist_relu_3_100"))

run_mnist_all_top1_new("$(cur_dir)/../benchmarks/mnist-retrained-new-label","$(cur_dir)/../experiments","VeryDiff-New-Label",0, make_mnist_verifier_pruned_top1("_retrained"); timeout=120,benchmark_filter=(x->(IMAGE_FOCUS[parse(Int,split(x,"_")[2])+1]<=4)))

run_mnist_all_top1_new("$(cur_dir)/../benchmarks/mnist-retrained-new-label","$(cur_dir)/../experiments","NNEquiv-New-Label",0, make_mnist_verifier_pruned_top1_nnequiv("_retrained"); timeout=120,filter=x->contains(x,"-old.onnx"),
benchmark_filter=(x->(IMAGE_FOCUS[parse(Int,split(x,"_")[2])+1]<=3)))

run_mnist_all_top1_new("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments","NNEquiv-OneThread-3_100_0.79",0, make_mnist_verifier_pruned_top1_nnequiv("_0.79_adam_1_stepsize"); timeout=120,filter=x->contains(x,"mnist_relu_3_100-old"))

# Unfinished
#run_mnist_all_top1_new("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments","VeryDiff-OneThread-4_1024_0.79",0, make_mnist_verifier_pruned_top1("_0.79"); timeout=120,filter=x->contains(x,"mnist_relu_4_1024"))

#run_mnist_all_top1_new("$(cur_dir)/../benchmarks/mnist_separately_trained","$(cur_dir)/../experiments","VeryDiff-OneThread-sep_trained",0, make_mnist_verifier_pruned_top1(5); timeout=120,filter=x->true)

end
