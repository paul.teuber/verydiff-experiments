function warmup(;simple=false)
    println("Warmup/Test run...")
    
    cur_dir = @__DIR__
    
    println("############################################")
    println("WARMUP RUN VeryDiff (ACAS)")
    println("############################################")
    acas_network_truncated("$(cur_dir)/../benchmarks/acas/nets/ACASXU_run2a_1_1_batch_2000.onnx","$(cur_dir)/../benchmarks/acas-retrain/specs/prop_1.vnnlib",0.05;focus=true)
    acas_network_truncated("$(cur_dir)/../benchmarks/acas/nets/ACASXU_run2a_1_1_batch_2000.onnx","$(cur_dir)/../benchmarks/acas-retrain/specs/prop_1.vnnlib",0.05;focus=false)
    if !simple
        println("############################################")
        println("WARMUP RUN NeuroDIFF")
        println("############################################")
        acas_network_truncated_neurodiff("$(cur_dir)/../benchmarks/acas/nets/ACASXU_run2a_1_1_batch_2000.onnx","$(cur_dir)/../benchmarks/acas/specs/prop_1.vnnlib",0.05)
    end
    
    println("############################################")
    println("WARMUP RUN VeryDiff (MNIST Top 1)")
    println("############################################")
    mnist_network_truncated_top1("$(cur_dir)/../benchmarks/mnist/nets/mnist_relu_2_512.onnx","$(cur_dir)/../benchmarks/mnist/specs/mnist_0_local_15.vnnlib",0)
    
    if !simple
        println("############################################")
        println("WARMUP RUN NNEquiv (MNIST Top 1)")
        println("############################################")
        (make_mnist_verifier_pruned_top1_nnequiv(5))("$(cur_dir)/../benchmarks/mnist-prune/nets/mnist_relu_2_512.onnx","$(cur_dir)/../benchmarks/mnist-prune/specs/mnist_0_global_3.vnnlib",0)
    
        println("############################################")
        println("WARMUP RUN MILPEquiv (MNIST Top 1)")
        println("############################################")
        try
            (make_mnist_verifier_pruned_top1_milpequiv(5))("$(cur_dir)/../benchmarks/mnist-prune/nets/mnist_relu_2_512.onnx","$(cur_dir)/../benchmarks/mnist-prune/specs/mnist_0_global_3.vnnlib",0;timeout=20)
        catch
            println("MILPEQUIV failed -> maybe no Gurobi?")
            println("############################################")
        end
    
        println("############################################")
        println("WARMUP RUN NNEquiv (MNIST Epsilon)")
        println("############################################")
        (make_mnist_verifier_pruned_epsilon_nnequiv(5))("$(cur_dir)/../benchmarks/mnist-prune/nets/mnist_relu_2_512.onnx","$(cur_dir)/../benchmarks/mnist-prune/specs/mnist_0_global_3.vnnlib",1.0)
    
        println("############################################")
        println("WARMUP RUN MILPEquiv (MNIST Epsilon)")
        println("############################################")
        try
            (make_mnist_verifier_pruned_epsilon_milpequiv(5,false))("$(cur_dir)/../benchmarks/mnist-prune/nets/mnist_relu_2_512.onnx","$(cur_dir)/../benchmarks/mnist-prune/specs/mnist_0_global_3.vnnlib",1.0;timeout=20)
        catch
            println("MILPEQUIV failed -> maybe no Gurobi?")
            println("############################################")
        end
    end
end