using VNNLib

IMAGE_FOCUS = Int[7,2,1,0,4,1,4,9,5,9,0,6,9,0,1,5,9,7,3,4,9,6,6,5,4,0,7,4,0,1,3,1,3,4,7,2,7,1,2,1,1,7,4,2,3,5,1,2,4,4,6,3,5,5,6,0,4,1,9,5,7,8,9,3,7,4,6,4,3,0,7,0,2,9,1,7,3,2,9,7,7,6,2,7,8,4,7,3,6,1,3,6,9,3,1,4,1,7,6,9]

function mnist_network_truncated_inner(network, bounds, property_check, split_heuristic;timeout=1800)
    N32 = VeryDiff.truncate_network(Float32,network)
    N16 = VeryDiff.truncate_network(Float16,network)

    verify_network(N16,N32,bounds, property_check, split_heuristic; timeout=timeout)
end

function mnist_network_mirror_inner(network1, network2, bounds, property_check, split_heuristic;timeout=1800)

    verify_network(network1,network2,bounds, property_check, split_heuristic; timeout=timeout)
end

function mnist_network_truncated_epsilon(network_file, vnnlib_file, epsilon;focus=false)
    network = parse_network(VNNLib.load_network(network_file))
    f, n_inputs, _ = get_ast(vnnlib_file)
    focus_dim = nothing
    if focus
        prop = basename(vnnlib_file)
        image_num = parse(Int,split(prop,"_")[2])+1
        focus_dim = IMAGE_FOCUS[image_num]+1
    end

    property_check = get_epsilon_property(epsilon;focus_dim=focus_dim)
    split_heuristic = epsilon_split_heuristic

    for (bounds, _, _, num) in f
        mnist_network_truncated_inner(network, bounds[1:n_inputs,:], property_check,split_heuristic)
    end
end

function make_mnist_verifier_pruned(pruning;naive=false,quantitative=false)
    if pruning in [5,10,30]
        pruning = "_pruned$pruning"
    end
    return (network_file, vnnlib_file, epsilon; focus=false) -> begin
        network1 = parse_network(VNNLib.load_network(network_file))
        network_file2 = replace(network_file, ".onnx" => "$pruning.onnx", "/nets/" => "/nets_pruned/")
        network2 = parse_network(VNNLib.load_network(network_file2))
        f, n_inputs, _ = get_ast(vnnlib_file)
        focus_dim = nothing
        if focus
            prop = basename(vnnlib_file)
            image_num = parse(Int,split(prop,"_")[2])+1
            focus_dim = IMAGE_FOCUS[image_num]+1
        end

        for (bounds, _, _, num) in f
            property_check = if naive
                get_epsilon_property_naive(epsilon;focus_dim=focus_dim)
            else
                get_epsilon_property(epsilon;focus_dim=focus_dim)
            end
            split_heuristic = epsilon_split_heuristic
            verify_network(network1, network2, bounds[1:n_inputs,:], property_check, split_heuristic;timeout=120)
        end
    end
end

function mnist_network_truncated_top1(network_file, vnnlib_file, mode;timeout=1800)
    network = parse_network(VNNLib.load_network(network_file))
    f, n_inputs, _ = get_ast(vnnlib_file)

    property_check = get_top1_property() #scale=1.05)
    split_heuristic = top1_configure_split_heuristic(mode)

    for (bounds, _, _, num) in f
        mnist_network_truncated_inner(network, bounds[1:n_inputs,:], property_check,split_heuristic;timeout=timeout)
    end
end

function make_mnist_verifier_pruned_top1(pruning;naive=false)
    if pruning in [5,10,30]
        pruning = "_pruned$pruning"
    end
    return (network_file, vnnlib_file, mode; timeout=120, delta=zero(Float64)) -> begin
        println("Parsing $(network_file)...")
        network1 = parse_network(VNNLib.load_network(network_file))
        network_file2 = replace(network_file, ".onnx" => "$pruning.onnx", "/nets/" => "/nets_pruned/")
        println("Parsing $(network_file2)...")
        network2 = parse_network(VNNLib.load_network(network_file2))
        f, n_inputs, _ = get_ast(vnnlib_file)

        for (bounds, _, _, num) in f
            property_check = get_top1_property(naive=naive, delta=delta) #scale=1.05)
            split_heuristic = top1_configure_split_heuristic(mode)
            verify_network(network1, network2, bounds[1:n_inputs,:], property_check, split_heuristic;timeout=timeout)
        end
    end
end

function mnist_network_mirror_top1(network_file1, vnnlib_file, mode;timeout=1800)
    network_file2 = replace(network_file1, ".onnx"=>"-mirror.onnx")
    network1 = parse_network(VNNLib.load_network(network_file1))
    network2 = parse_network(VNNLib.load_network(network_file2))
    f, n_inputs, _ = get_ast(vnnlib_file)

    property_check = get_top1_property() #scale=1.05)
    split_heuristic = top1_configure_split_heuristic(mode)

    for (bounds, _, _, num) in f
        mnist_network_mirror_inner(network1, network2, bounds[1:n_inputs,:], property_check,split_heuristic;timeout=timeout)
    end
end

function mnist_network_mirror_top1_nnequiv(network_file, vnnlib_file, mode;timeout=1800)
    # Networks: 

    n1_file = network_file
    n2_file = replace(network_file, ".onnx"=>"-mirror.onnx")

    println("Loading $(n1_file) and $(n2_file)...")

    f, n_inputs, _ = get_ast(vnnlib_file)

    for (bounds, _, _, num) in f
        runtime = @elapsed nnequiv_verify(n1_file, n2_file, bounds, "top", timeout)
        println("time: $(runtime)")
    end
end

function make_mnist_verifier_pruned_top1_nnequiv(pruning)
    if pruning in [5,10,30]
        pruning = "_pruned$pruning"
    end
    return (network_file1, vnnlib_file, mode; timeout=120) -> begin
        network_file2 = replace(network_file1, ".onnx" => "$pruning.onnx", "/nets/" => "/nets_pruned/")
        f, n_inputs, _ = get_ast(vnnlib_file)

        for (bounds, _, _, num) in f
            runtime = @elapsed nnequiv_verify(network_file1, network_file2, bounds, "top", timeout)
            println("time: $(runtime)")
        end
    end
end

function make_mnist_verifier_pruned_epsilon_nnequiv(pruning)
    if pruning in [5,10,30]
        pruning = "_pruned$pruning"
    end
    return (network_file1, vnnlib_file, epsilon; timeout=120, focus=false) -> begin
        @assert !focus
        network_file2 = replace(network_file1, ".onnx" => "$pruning.onnx", "/nets/" => "/nets_pruned/")
        f, n_inputs, _ = get_ast(vnnlib_file)

        for (bounds, _, _, num) in f
            runtime = @elapsed nnequiv_verify(network_file1, network_file2, bounds, epsilon, timeout)
            println("time: $(runtime)")
        end
    end
end

function mnist_network_truncated_top1_nnequiv(network_file, vnnlib_file, mode;timeout=1800)
    # Networks: 

    n1_file = network_file
    n2_file = replace(network_file, "/nets/" => "/nets_compressed/")

    println("Loading $(n1_file) and $(n2_file)...")

    f, n_inputs, _ = get_ast(vnnlib_file)

    for (bounds, _, _, num) in f
        runtime = @elapsed nnequiv_verify(n1_file, n2_file, bounds, "top", timeout)
        println("time: $(runtime)")
    end
end

function mnist_network_truncated_top1_milpequiv(network_file, vnnlib_file, mode;timeout=1800)
    # Networks: 

    n1_file = network_file
    n2_file = replace(network_file, "/nets/" => "/nets_compressed/")

    println("Loading $(n1_file) and $(n2_file)...")

    f, n_inputs, _ = get_ast(vnnlib_file)

    for (bounds, _, _, num) in f
        bounds_tightening = (mode==1)
        runtime = @elapsed milpequiv_verify(n1_file, n2_file, bounds, "top", timeout,bounds_tightening)
        println("time: $(runtime)")
    end
end

function make_mnist_verifier_pruned_top1_milpequiv(pruning)
    @assert pruning in [5,10,30]
    return (network_file1, vnnlib_file, mode; timeout=120) -> begin
        network_file2 = replace(network_file1, ".onnx" => "_pruned$pruning.onnx", "/nets/" => "/nets_pruned/")
        f, n_inputs, _ = get_ast(vnnlib_file)

        for (bounds, _, _, num) in f
            bounds_tightening = (mode==1)
            runtime = @elapsed milpequiv_verify(network_file1, network_file2, bounds, "top", timeout,bounds_tightening)
            println("time: $(runtime)")
        end
    end
end

function make_mnist_verifier_pruned_epsilon_milpequiv(pruning, mode)
    @assert pruning in [5,10,30]
    return (network_file1, vnnlib_file, epsilon; timeout=120, focus=false) -> begin
        @assert !focus
        network_file2 = replace(network_file1, ".onnx" => "_pruned$pruning.onnx", "/nets/" => "/nets_pruned/")
        f, n_inputs, _ = get_ast(vnnlib_file)

        for (bounds, _, _, num) in f
            bounds_tightening = mode
            runtime = @elapsed milpequiv_verify(network_file1, network_file2, bounds, epsilon, timeout,bounds_tightening)
            println("time: $(runtime)")
        end
    end
end

function mnist_network_truncated_neurodiff(network_file, vnnlib_file, epsilon;focus=false)
    spec_name = basename(vnnlib_file)
    network_name = split(basename(network_file),".")[1]
    spec_parts = split(split(spec_name,".")[1], "_")
    image_num = parse(Int,spec_parts[2])+400
    prop_type = spec_parts[3]
    property = spec_parts[4]
    path = "../NeuroDiff/DiffNN-Code"
    if prop_type == "global"
        command = `timeout -k 5 1800 $path/delta_network_test $image_num $path/nnet/$network_name.nnet $path/compressed_nnets/$(network_name)_16bit.nnet $epsilon -p $property`
    elseif prop_type == "local"
        command = `timeout -k 5 1800 $path/delta_network_test $image_num $path/nnet/$network_name.nnet $path/compressed_nnets/$(network_name)_16bit.nnet $epsilon -x $property`
    else
        throw("Unknown property type $prop_type")
    end
    command = setenv(command, ("OPENBLAS_NUM_THREADS" => "1"))
    command = setenv(command, ("OMP_NUM_THREADS" => "1"))
    command = setenv(command, ("LD_LIBRARY_PATH" => ENV["LD_LIBRARY_PATH"]))
    print(command)
    try
        println(run(command))
    catch exc
        println("Caught exception: $exc")
        showerror(stdout, exc)
    end
end

function make_mnist_verifier_pruned_neurodiff(pruning)
    if pruning in [5,10,30]
        pruning = "_pruned$pruning"
    end
    return (network_file, vnnlib_file, epsilon; focus=false) -> begin
        network_name = split(basename(network_file),".")[1]
        network_dir = dirname(network_file)
        network1 = joinpath(network_dir, network_name*".nnet")
        network2 = joinpath(replace(network_dir, "nets" => "nets_pruned"), network_name*"$pruning.nnet")
        
        spec_name = basename(vnnlib_file)
        spec_parts = split(split(spec_name,".")[1], "_")
        image_num = parse(Int,spec_parts[2])+400
        prop_type = spec_parts[3]
        property = spec_parts[4]
    
        path = "../NeuroDiff/DiffNN-Code"
        if prop_type == "global"
            command = `timeout -k 5 120 $path/delta_network_test $image_num $network1 $network2 $epsilon -p $property`
        elseif prop_type == "local"
            command = `timeout -k 5 120 $path/delta_network_test $image_num $network1 $network2 $epsilon -x $property`
        else
            throw("Unknown property type $prop_type")
        end
        command = setenv(command, ("OPENBLAS_NUM_THREADS" => "1"))
        command = setenv(command, ("OMP_NUM_THREADS" => "1"))
        command = setenv(command, ("LD_LIBRARY_PATH" => ENV["LD_LIBRARY_PATH"]))
        print(command)
        res = nothing
    try
        res = run(command)
    catch e
        println("TIMEOUT")
        println(e)
    finally
        println(res)
    end
    end
end

function run_mnist_all(benchmark_dir, log_dir, run_name, epsilon, run_eval_function;focus=false,filter=x->true,benchmark_filter=(x->true))
    log_dir = joinpath(log_dir, "mnist-$epsilon", run_name)
    mkpath(log_dir)
    original_stdout = stdout
    original_stderr = stderr
    spec_paths = joinpath.(benchmark_dir, "specs", readdir(joinpath(benchmark_dir,"specs")))
    benchmark_paths = joinpath.(benchmark_dir,"nets", readdir(joinpath(benchmark_dir,"nets")))
    total_time = @elapsed for spec_file in spec_paths
        spec_name = basename(spec_file)
        log_file_name = joinpath(log_dir,"$spec_name.log")
        if !benchmark_filter(spec_file)
            continue
        end
        if isfile(log_file_name)
            continue
        else
            open(log_file_name, "w") do f
                redirect_stdout(f)
                redirect_stderr(f)
                spec_time = @elapsed begin
                    for net_file in benchmark_paths
                        if !endswith(net_file,".onnx") || !filter(net_file)
                            continue
                        end
                        println("\n[VERYDIFF_EXPERIMENT] Running on $spec_file $net_file")
                        flush(stdout)
                        flush(stderr)
                        cur_runtime = @elapsed run_eval_function(net_file, spec_file, epsilon,focus=focus)
                        flush(stdout)
                        flush(stderr)
                        println("")
                        println("[VERYDIFF_TIMER] $cur_runtime")
                        GC.gc()
                    end
                end
                println("\n[VERYDIFF_EXPERIMENT] NETWORK: $spec_file")
                println("[VERYDIFF_EXPERIMENT] TIME: $spec_time")
                redirect_stdout(original_stdout)
                redirect_stderr(original_stderr)
            end
        end
    end
    println("\n[VERYDIFF_EXPERIMENT] Total time: $total_time")
end

function run_mnist_all_top1(benchmark_dir, log_dir, run_name, mode, run_eval_function;timeout=1800,benchmark_filter=(x->true))
    log_dir = joinpath(log_dir, "mnist-top1", run_name)
    mkpath(log_dir)
    original_stdout = stdout
    original_stderr = stderr
    spec_paths = joinpath.(benchmark_dir, "specs", readdir(joinpath(benchmark_dir,"specs")))
    benchmark_paths = joinpath.(benchmark_dir,"nets", readdir(joinpath(benchmark_dir,"nets")))
    total_time = @elapsed for spec_file in spec_paths
        if contains(spec_file,"_global_6") || contains(spec_file,"_local_24")
            continue
        end
        spec_name = basename(spec_file)
        log_file_name = joinpath(log_dir,"$spec_name.log")
        if !benchmark_filter(spec_file)
            continue
        end
        if isfile(log_file_name)
            continue
        else
            open(log_file_name, "w") do f
                redirect_stdout(f)
                redirect_stderr(f)
                spec_time = @elapsed begin
                    for net_file in benchmark_paths
                        if endswith(net_file,".nnet")
                            continue
                        end
                        println("\n[VERYDIFF_EXPERIMENT] Running on $spec_file $net_file")
                        flush(stdout)
                        flush(stderr)
                        cur_runtime = @elapsed run_eval_function(net_file, spec_file, mode; timeout=timeout)
                        flush(stdout)
                        flush(stderr)
                        println("")
                        println("[VERYDIFF_TIMER] $cur_runtime")
                        GC.gc()
                    end
                end
                println("\n[VERYDIFF_EXPERIMENT] NETWORK: $spec_file")
                println("[VERYDIFF_EXPERIMENT] TIME: $spec_time")
                redirect_stdout(original_stdout)
                redirect_stderr(original_stderr)
            end
        end
    end
    println("\n[VERYDIFF_EXPERIMENT] Total time: $total_time")
end

function run_mnist_all_top1_new(benchmark_dir, log_dir, run_name, mode, run_eval_function;timeout=1800,filter=(x -> true),benchmark_filter=(x->true))
    log_dir = joinpath(log_dir, "mnist-top1", run_name)
    mkpath(log_dir)
    original_stdout = stdout
    original_stderr = stderr
    spec_paths = joinpath.(benchmark_dir, "specs", readdir(joinpath(benchmark_dir,"specs")))
    benchmark_paths = joinpath.(benchmark_dir,"nets", readdir(joinpath(benchmark_dir,"nets")))
    total_time = @elapsed for spec_file in spec_paths
        if contains(spec_file,"_global_6") || contains(spec_file,"_local_24")
            continue
        end
        if !benchmark_filter(spec_file)
            continue
        end
        spec_name = basename(spec_file)
        log_file_name = joinpath(log_dir,"$spec_name.log")
        if isfile(log_file_name)
            continue
        else
            open(log_file_name, "w") do f
                redirect_stdout(f)
                redirect_stderr(f)
                spec_time = @elapsed begin
                    for net_file in benchmark_paths
                        if !filter(net_file) || endswith(net_file,".nnet")
                            continue
                        end
                        println("\n[VERYDIFF_EXPERIMENT] Running on $spec_file $net_file")
                        flush(stdout)
                        flush(stderr)
                        cur_runtime = @elapsed run_eval_function(net_file, spec_file, mode; timeout=timeout)
                        flush(stdout)
                        flush(stderr)
                        println("")
                        println("[VERYDIFF_TIMER] $cur_runtime")
                        GC.gc()
                    end
                end
                println("\n[VERYDIFF_EXPERIMENT] NETWORK: $spec_file")
                println("[VERYDIFF_EXPERIMENT] TIME: $spec_time")
                redirect_stdout(original_stdout)
                redirect_stderr(original_stderr)
            end
        end
    end
    println("\n[VERYDIFF_EXPERIMENT] Total time: $total_time")
end