function run_experiments_updated()

    warmup()
    cur_dir = @__DIR__
    old_value = VeryDiff.NEW_HEURISTIC

    VeryDiff.NEW_HEURISTIC=true
    VeryDiffExperiments.run_acas_all("$(cur_dir)/../benchmarks/acas-prune","$(cur_dir)/../experiments_abelation","VeryDiff-OneThread-5-NEW",0.05, VeryDiffExperiments.make_acas_verifier_pruned(5);focus=false)
    VeryDiffExperiments.run_acas_all("$(cur_dir)/../benchmarks/acas-prune","$(cur_dir)/../experiments_abelation","VeryDiff-OneThread-Focus-5-NEW",0.05, VeryDiffExperiments.make_acas_verifier_pruned(5);focus=true)
    VeryDiffExperiments.run_acas_all("$(cur_dir)/../benchmarks/acas-prune","$(cur_dir)/../experiments_abelation","VeryDiff-OneThread-10-NEW",0.05, VeryDiffExperiments.make_acas_verifier_pruned(10);focus=false)
    VeryDiffExperiments.run_acas_all("$(cur_dir)/../benchmarks/acas-prune","$(cur_dir)/../experiments_abelation","VeryDiff-OneThread-Focus-10-NEW",0.05, VeryDiffExperiments.make_acas_verifier_pruned(10);focus=true)
    VeryDiffExperiments.run_acas_all("$(cur_dir)/../benchmarks/acas-prune","$(cur_dir)/../experiments_abelation","VeryDiff-OneThread-30-NEW",0.05, VeryDiffExperiments.make_acas_verifier_pruned(30);focus=false)
    VeryDiffExperiments.run_acas_all("$(cur_dir)/../benchmarks/acas-prune","$(cur_dir)/../experiments_abelation","VeryDiff-OneThread-Focus-30-NEW",0.05, VeryDiffExperiments.make_acas_verifier_pruned(30);focus=true)

    run_mnist_all("$(cur_dir)/../benchmarks/mnist-prune-small","$(cur_dir)/../experiments_abelation","VeryDiff-OneThread-5-NEW",1.0, make_mnist_verifier_pruned(5);focus=false)
    #run_mnist_all("$(cur_dir)/../benchmarks/mnist-prune-small","$(cur_dir)/../experiments","VeryDiff-OneThread-Focus-5-NEW",1.0, make_mnist_verifier_pruned(5);focus=true)

    VeryDiff.NEW_HEURISTIC=false
    VeryDiffExperiments.run_acas_all("$(cur_dir)/../benchmarks/acas-prune","$(cur_dir)/../experiments_abelation","VeryDiff-OneThread-5-OLD",0.05, VeryDiffExperiments.make_acas_verifier_pruned(5);focus=false)
    VeryDiffExperiments.run_acas_all("$(cur_dir)/../benchmarks/acas-prune","$(cur_dir)/../experiments_abelation","VeryDiff-OneThread-Focus-5-OLD",0.05, VeryDiffExperiments.make_acas_verifier_pruned(5);focus=true)
    VeryDiffExperiments.run_acas_all("$(cur_dir)/../benchmarks/acas-prune","$(cur_dir)/../experiments_abelation","VeryDiff-OneThread-10-OLD",0.05, VeryDiffExperiments.make_acas_verifier_pruned(10);focus=false)
    VeryDiffExperiments.run_acas_all("$(cur_dir)/../benchmarks/acas-prune","$(cur_dir)/../experiments_abelation","VeryDiff-OneThread-Focus-10-OLD",0.05, VeryDiffExperiments.make_acas_verifier_pruned(10);focus=true)
    VeryDiffExperiments.run_acas_all("$(cur_dir)/../benchmarks/acas-prune","$(cur_dir)/../experiments_abelation","VeryDiff-OneThread-30-OLD",0.05, VeryDiffExperiments.make_acas_verifier_pruned(30);focus=false)
    VeryDiffExperiments.run_acas_all("$(cur_dir)/../benchmarks/acas-prune","$(cur_dir)/../experiments_abelation","VeryDiff-OneThread-Focus-30-OLD",0.05, VeryDiffExperiments.make_acas_verifier_pruned(30);focus=true)

    run_mnist_all("$(cur_dir)/../benchmarks/mnist-prune-small","$(cur_dir)/../experiments_abelation","VeryDiff-OneThread-5-OLD",1.0, make_mnist_verifier_pruned(5);focus=false)
    #run_mnist_all("$(cur_dir)/../benchmarks/mnist-prune-small","$(cur_dir)/../experiments","VeryDiff-OneThread-Focus-5-OLD",1.0, make_mnist_verifier_pruned(5);focus=true)

    VeryDiff.NEW_HEURISTIC=old_value
    
end
    