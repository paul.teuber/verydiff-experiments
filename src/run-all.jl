function run_all_experiments()

    warmup()

    cur_dir = @__DIR__
    
    println("Running ACAS experiments...")

    println("1) NETWORKS BY VeryPrune")
    
    println("1.1) PRUNED ACAS (Epsilon)")
    
    println("1.1.1) NEURODIFF")
    
    run_acas_all("$(cur_dir)/../benchmarks/acas-prune","$(cur_dir)/../experiments_final","NeuroDiff-OneThread-5",0.05, make_acas_verifier_pruned_neurodiff(5);focus=true)
    run_acas_all("$(cur_dir)/../benchmarks/acas-prune","$(cur_dir)/../experiments_final","NeuroDiff-OneThread-10",0.05, make_acas_verifier_pruned_neurodiff(10);focus=true)
    run_acas_all("$(cur_dir)/../benchmarks/acas-prune","$(cur_dir)/../experiments_final","NeuroDiff-OneThread-30",0.05, make_acas_verifier_pruned_neurodiff(30);focus=true)
    
    println("1.1.2) NNEQUIV")
    
    run_acas_all("$(cur_dir)/../benchmarks/acas-prune","$(cur_dir)/../experiments_final","NNEquiv-OneThread-5",0.05, make_mnist_verifier_pruned_epsilon_nnequiv(5); focus=false)
    run_acas_all("$(cur_dir)/../benchmarks/acas-prune","$(cur_dir)/../experiments_final","NNEquiv-OneThread-10",0.05, make_mnist_verifier_pruned_epsilon_nnequiv(10); focus=false)
    run_acas_all("$(cur_dir)/../benchmarks/acas-prune","$(cur_dir)/../experiments_final","NNEquiv-OneThread-30",0.05, make_mnist_verifier_pruned_epsilon_nnequiv(30); focus=false)

    println("1.1.3) MILPEQUIV")
    run_acas_all("$(cur_dir)/../benchmarks/acas-prune","$(cur_dir)/../experiments_final","MILPEquiv-OneThread-5",0.05, make_mnist_verifier_pruned_epsilon_milpequiv(5, false); focus=false)
    #run_acas_all("$(cur_dir)/../benchmarks/acas-prune","$(cur_dir)/../experiments_final","MILPEquiv-OneThread-BoundsTightening-5",0.05, make_mnist_verifier_pruned_epsilon_milpequiv(5, true); focus=false)
    run_acas_all("$(cur_dir)/../benchmarks/acas-prune","$(cur_dir)/../experiments_final","MILPEquiv-OneThread-10",0.05, make_mnist_verifier_pruned_epsilon_milpequiv(10, false); focus=false)
    # run_acas_all("$(cur_dir)/../benchmarks/acas-prune","$(cur_dir)/../experiments_final","MILPEquiv-OneThread-BoundsTightening-10",0.05, make_mnist_verifier_pruned_epsilon_milpequiv(10, true); focus=false)
    run_acas_all("$(cur_dir)/../benchmarks/acas-prune","$(cur_dir)/../experiments_final","MILPEquiv-OneThread-30",0.05, make_mnist_verifier_pruned_epsilon_milpequiv(30, false); focus=false)
    # run_acas_all("$(cur_dir)/../benchmarks/acas-prune","$(cur_dir)/../experiments_final","MILPEquiv-OneThread-BoundsTightening-30",0.05, make_mnist_verifier_pruned_epsilon_milpequiv(30, true); focus=false)

    println("1.1.4) VeryDiff")
    run_acas_all("$(cur_dir)/../benchmarks/acas-prune","$(cur_dir)/../experiments_final","VeryDiff-OneThread-5-Combined",0.05, make_acas_verifier_pruned(5); focus=false)
    run_acas_all("$(cur_dir)/../benchmarks/acas-prune","$(cur_dir)/../experiments_final","VeryDiff-OneThread-Focus-5-Combined",0.05, make_acas_verifier_pruned(5); focus=true)
    run_acas_all("$(cur_dir)/../benchmarks/acas-prune","$(cur_dir)/../experiments_final","VeryDiff-OneThread-10-Combined",0.05, make_acas_verifier_pruned(10); focus=false)
    run_acas_all("$(cur_dir)/../benchmarks/acas-prune","$(cur_dir)/../experiments_final","VeryDiff-OneThread-Focus-10-Combined",0.05, make_acas_verifier_pruned(10); focus=true)
    run_acas_all("$(cur_dir)/../benchmarks/acas-prune","$(cur_dir)/../experiments_final","VeryDiff-OneThread-30-Combined",0.05, make_acas_verifier_pruned(30); focus=false)
    run_acas_all("$(cur_dir)/../benchmarks/acas-prune","$(cur_dir)/../experiments_final","VeryDiff-OneThread-Focus-30-Combined",0.05, make_acas_verifier_pruned(30); focus=true)

    println("1.1.4) VeryDiff (NAIVE)")
    VeryDiff.USE_DIFFZONO = false
    run_acas_all("$(cur_dir)/../benchmarks/acas-prune","$(cur_dir)/../experiments_final","VeryDiff-OneThread-5-Naive",0.05, make_acas_verifier_pruned(5;naive=true); focus=false)
    run_acas_all("$(cur_dir)/../benchmarks/acas-prune","$(cur_dir)/../experiments_final","VeryDiff-OneThread-Focus-5-Naive",0.05, make_acas_verifier_pruned(5;naive=true); focus=true)
    run_acas_all("$(cur_dir)/../benchmarks/acas-prune","$(cur_dir)/../experiments_final","VeryDiff-OneThread-10-Naive",0.05, make_acas_verifier_pruned(10;naive=true); focus=false)
    run_acas_all("$(cur_dir)/../benchmarks/acas-prune","$(cur_dir)/../experiments_final","VeryDiff-OneThread-Focus-10-Naive",0.05, make_acas_verifier_pruned(10;naive=true); focus=true)
    run_acas_all("$(cur_dir)/../benchmarks/acas-prune","$(cur_dir)/../experiments_final","VeryDiff-OneThread-30-Naive",0.05, make_acas_verifier_pruned(30;naive=true); focus=false)
    run_acas_all("$(cur_dir)/../benchmarks/acas-prune","$(cur_dir)/../experiments_final","VeryDiff-OneThread-Focus-30-Naive",0.05, make_acas_verifier_pruned(30;naive=true); focus=true)
    VeryDiff.USE_DIFFZONO = true


    println("1.2) PRUNED MNIST (Epsilon)")
    
    println("1.2.1) NEURODIFF")
    run_mnist_all("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments_final","NeuroDiff-OneThread-Focus-5",1.0, make_mnist_verifier_pruned_neurodiff(5);focus=true)

    println("1.2.2) NNEQUIV")
    run_mnist_all("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments_final","NNEquiv-OneThread-5",1.0, make_mnist_verifier_pruned_epsilon_nnequiv(5); focus=false)

    println("1.2.3) MILPEQUIV")
    run_mnist_all("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments_final","MILPEquiv-OneThread-5",1.0, make_mnist_verifier_pruned_epsilon_milpequiv(5,false); focus=false, benchmark_filter=(x->(parse(Int, split(x,"_")[2])<50)))
    #run_mnist_all("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments_final","MILPEquiv-OneThread-BoundsTightening-5",1.0, make_mnist_verifier_pruned_epsilon_milpequiv(5,true); focus=false, benchmark_filter=(x->(parse(Int, split(x,"_")[2])<50)))

    println("1.2.4) VeryDiff")
    run_mnist_all("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments_final","VeryDiff-OneThread-5-Combined",1.0, make_mnist_verifier_pruned(5); focus=false)
    run_mnist_all("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments_final","VeryDiff-OneThread-Focus-5-Combined",1.0, make_mnist_verifier_pruned(5); focus=true)

    println("1.2.4) VeryDiff (NAIVE)")
    VeryDiff.USE_DIFFZONO = false
    run_mnist_all("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments_final","VeryDiff-OneThread-5-Naive",1.0, make_mnist_verifier_pruned(5;naive=true); focus=false)
    run_mnist_all("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments_final","VeryDiff-OneThread-Focus-5-Naive",1.0, make_mnist_verifier_pruned(5;naive=true); focus=true)
    VeryDiff.USE_DIFFZONO = true


    println("1.3) PRUNED MNIST (Top-1)")
    
    println("1.3.1) NEURODIFF")
    println("SKIPPED")

    println("1.3.2) NNEQUIV")
    run_mnist_all_top1("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments_final","NNEquiv-OneThread-5",0, make_mnist_verifier_pruned_top1_nnequiv(5); timeout=120)

    println("1.3.3) MILPEQUIV")
    run_mnist_all_top1("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments_final","MILPEquiv-OneThread-5",0, make_mnist_verifier_pruned_top1_milpequiv(5); timeout=120, benchmark_filter=(x->(parse(Int, split(x,"_")[2])<50)))
    #run_mnist_all_top1("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments_final","MILPEquiv-OneThread-BoundsTightening-5",1, make_mnist_verifier_pruned_top1_milpequiv(5); timeout=120, benchmark_filter=(x->(parse(Int, split(x,"_")[2])<50)))

    println("1.3.4) VeryDiff")
    run_mnist_all_top1_new("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments_final","VeryDiff-OneThread-5-Combined",1, make_mnist_verifier_pruned_top1(5); timeout=120, filter=x->!contains(x,"mnist_relu_4_1024"))

    println("1.3.4) VeryDiff (NAIVE)")
    VeryDiff.USE_DIFFZONO = false
    run_mnist_all_top1_new("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments_final","VeryDiff-OneThread-5-Naive",1, make_mnist_verifier_pruned_top1(5;naive=true); timeout=120, filter=x->!contains(x,"mnist_relu_4_1024"))
    VeryDiff.USE_DIFFZONO = true

    println("2) NETWORKS FROM US")

    println("2.2) PRUNED MNIST (Top-1)")

    println("2.2.1) NEURODIFF")
    println("SKIPPED")

    println("2.2.2) NNEQUIV")
    run_mnist_all_top1_new("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments_final","NNEquiv-OneThread-2_512_0.79", 0, make_mnist_verifier_pruned_top1_nnequiv("_0.79-old"); timeout=120, filter=x->contains(x,"mnist_relu_2_512"))

    run_mnist_all_top1_new("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments_final","NNEquiv-OneThread-3_100_0.49", 0, make_mnist_verifier_pruned_top1_nnequiv("_0.49-old"); timeout=120, filter=x->contains(x,"mnist_relu_3_100"))

    run_mnist_all_top1_new("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments_final","NNEquiv-OneThread-3_100_0.77", 0, make_mnist_verifier_pruned_top1_nnequiv("_0.77-old"); timeout=120, filter=x->contains(x,"mnist_relu_3_100"))

    run_mnist_all_top1_new("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments_final","NNEquiv-OneThread-3_100_0.79", 0, make_mnist_verifier_pruned_top1_nnequiv("_0.79_adam_1_stepsize-old"); timeout=120, filter=x->contains(x,"mnist_relu_3_100"))

    run_mnist_all_top1_new("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments_final","NNEquiv-OneThread-4_1024_0.79", 0, make_mnist_verifier_pruned_top1_nnequiv("_0.79-old"); timeout=120, filter=x->contains(x,"mnist_relu_4_1024"))

    println("2.2.2) VeryDiff")
    run_mnist_all_top1_new("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments_final","VeryDiff-OneThread-2_512_0.79-Combined", 1, make_mnist_verifier_pruned_top1("_0.79-old"); timeout=120, filter=x->contains(x,"mnist_relu_2_512"))

    run_mnist_all_top1_new("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments_final","VeryDiff-OneThread-3_100_0.49-Combined", 1, make_mnist_verifier_pruned_top1("_0.49-old"); timeout=120, filter=x->contains(x,"mnist_relu_3_100"))

    run_mnist_all_top1_new("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments_final","VeryDiff-OneThread-3_100_0.77-Combined", 1, make_mnist_verifier_pruned_top1("_0.77-old"); timeout=120, filter=x->contains(x,"mnist_relu_3_100"))

    run_mnist_all_top1_new("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments_final","VeryDiff-OneThread-3_100_0.79-Combined", 1, make_mnist_verifier_pruned_top1("_0.79_adam_1_stepsize-old"); timeout=120, filter=x->contains(x,"mnist_relu_3_100"))

    run_mnist_all_top1_new("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments_final","VeryDiff-OneThread-4_1024_0.79-Combined", 1, make_mnist_verifier_pruned_top1("_0.79-old"); timeout=120, filter=x->contains(x,"mnist_relu_4_1024"))

    print("2.2.3) VeryDiff (Naive)")
    VeryDiff.USE_DIFFZONO = false
    run_mnist_all_top1_new("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments_final","VeryDiff-OneThread-2_512_0.79-Naive", 1, make_mnist_verifier_pruned_top1("_0.79-old";naive=true); timeout=120, filter=x->contains(x,"mnist_relu_2_512"))

    run_mnist_all_top1_new("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments_final","VeryDiff-OneThread-3_100_0.49-Naive", 1, make_mnist_verifier_pruned_top1("_0.49-old";naive=true); timeout=120, filter=x->contains(x,"mnist_relu_3_100"))

    run_mnist_all_top1_new("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments_final","VeryDiff-OneThread-3_100_0.77-Naive", 1, make_mnist_verifier_pruned_top1("_0.77-old";naive=true); timeout=120, filter=x->contains(x,"mnist_relu_3_100"))

    run_mnist_all_top1_new("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments_final","VeryDiff-OneThread-3_100_0.79-Naive", 1, make_mnist_verifier_pruned_top1("_0.79_adam_1_stepsize-old";naive=true); timeout=120, filter=x->contains(x,"mnist_relu_3_100"))

    run_mnist_all_top1_new("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments_final","VeryDiff-OneThread-4_1024_0.79-Naive", 1, make_mnist_verifier_pruned_top1("_0.79-old";naive=true); timeout=120, filter=x->contains(x,"mnist_relu_4_1024"))
    
    VeryDiff.USE_DIFFZONO = true

    println("2.2.3) VeryDiff (GLPK)")
    VeryDiff.USE_GUROBI = false

    run_mnist_all_top1_new("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments_final","VeryDiff-OneThread-GLPK-2_512_0.79", 1, make_mnist_verifier_pruned_top1("_0.79-old"); timeout=120, filter=x->contains(x,"mnist_relu_2_512"))
    run_mnist_all_top1_new("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments_final","VeryDiff-OneThread-GLPK-3_100_0.79", 1, make_mnist_verifier_pruned_top1("_0.79_adam_1_stepsize-old"); timeout=120, filter=x->contains(x,"mnist_relu_3_100"))

    #VeryDiff.USE_GUROBI = true
    include("src/gurobi_config.jl")

    println("2.1) PRUNED MNIST (Epsilon)")

    println("2.1.1) NEURODIFF")
    run_mnist_all("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments_final","NeuroDiff-OneThread-2_512_0.79", 1.0, make_mnist_verifier_pruned_neurodiff("_0.79"); filter=x->contains(x,"mnist_relu_2_512"))

    run_mnist_all("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments_final","NeuroDiff-OneThread-3_100_0.49", 1.0, make_mnist_verifier_pruned_neurodiff("_0.49"); filter=x->contains(x,"mnist_relu_3_100"))

    run_mnist_all("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments_final","NeuroDiff-OneThread-3_100_0.77", 1.0, make_mnist_verifier_pruned_neurodiff("_0.77"); filter=x->contains(x,"mnist_relu_3_100"))

    run_mnist_all("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments_final","NeuroDiff-OneThread-3_100_0.79", 1.0, make_mnist_verifier_pruned_neurodiff("_0.79_adam_1_stepsize"); filter=x->contains(x,"mnist_relu_3_100"))

    run_mnist_all("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments_final","NeuroDiff-OneThread-4_1024_0.79", 1.0, make_mnist_verifier_pruned_neurodiff("_0.79"); filter=x->contains(x,"mnist_relu_4_1024"))

    println("2.1.2) NNEQUIV")
    run_mnist_all("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments_final","NNEquiv-OneThread-2_512_0.79", 1.0, make_mnist_verifier_pruned_epsilon_nnequiv("_0.79-old"); filter=x->contains(x,"mnist_relu_2_512"))

    run_mnist_all("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments_final","NNEquiv-OneThread-3_100_0.49", 1.0, make_mnist_verifier_pruned_epsilon_nnequiv("_0.49-old"); filter=x->contains(x,"mnist_relu_3_100"))

    run_mnist_all("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments_final","NNEquiv-OneThread-3_100_0.77", 1.0, make_mnist_verifier_pruned_epsilon_nnequiv("_0.77-old"); filter=x->contains(x,"mnist_relu_3_100"))

    run_mnist_all("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments_final","NNEquiv-OneThread-3_100_0.79", 1.0, make_mnist_verifier_pruned_epsilon_nnequiv("_0.79_adam_1_stepsize-old"); filter=x->contains(x,"mnist_relu_3_100"))

    run_mnist_all("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments_final","NNEquiv-OneThread-4_1024_0.79", 1.0, make_mnist_verifier_pruned_epsilon_nnequiv("_0.79-old"); filter=x->contains(x,"mnist_relu_4_1024"))

    println("2.1.3) MILPEQUIV")
    println("SKIPPED")

    println("2.1.4) VeryDiff")
    run_mnist_all("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments_final","VeryDiff-OneThread-2_512_0.79-Combined", 1.0, make_mnist_verifier_pruned("_0.79-old"); filter=x->contains(x,"mnist_relu_2_512"))

    run_mnist_all("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments_final","VeryDiff-OneThread-3_100_0.49-Combined", 1.0, make_mnist_verifier_pruned("_0.49-old"); filter=x->contains(x,"mnist_relu_3_100"))

    run_mnist_all("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments_final","VeryDiff-OneThread-3_100_0.77-Combined", 1.0, make_mnist_verifier_pruned("_0.77-old"); filter=x->contains(x,"mnist_relu_3_100"))

    run_mnist_all("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments_final","VeryDiff-OneThread-3_100_0.79-Combined", 1.0, make_mnist_verifier_pruned("_0.79_adam_1_stepsize-old"); filter=x->contains(x,"mnist_relu_3_100"))

    run_mnist_all("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments_final","VeryDiff-OneThread-4_1024_0.79-Combined", 1.0, make_mnist_verifier_pruned("_0.79-old"); filter=x->contains(x,"mnist_relu_4_1024"))

    # Focused

    run_mnist_all("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments_final","VeryDiff-OneThread-Focus-2_512_0.79-Combined", 1.0, make_mnist_verifier_pruned("_0.79-old"); filter=x->contains(x,"mnist_relu_2_512"), focus=true)

    run_mnist_all("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments_final","VeryDiff-OneThread-Focus-3_100_0.49-Combined", 1.0, make_mnist_verifier_pruned("_0.49-old"); filter=x->contains(x,"mnist_relu_3_100"), focus=true)

    run_mnist_all("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments_final","VeryDiff-OneThread-Focus-3_100_0.77-Combined", 1.0, make_mnist_verifier_pruned("_0.77-old"); filter=x->contains(x,"mnist_relu_3_100"), focus=true)

    run_mnist_all("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments_final","VeryDiff-OneThread-Focus-3_100_0.79-Combined", 1.0, make_mnist_verifier_pruned("_0.79_adam_1_stepsize-old"); filter=x->contains(x,"mnist_relu_3_100"), focus=true)

    run_mnist_all("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments_final","VeryDiff-OneThread-Focus-4_1024_0.79-Combined", 1.0, make_mnist_verifier_pruned("_0.79-old"); filter=x->contains(x,"mnist_relu_4_1024"), focus=true)

    println("2.1.4) VeryDiff (Naive)")
    run_mnist_all("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments_final","VeryDiff-OneThread-2_512_0.79-Naive", 1.0, make_mnist_verifier_pruned("_0.79-old";naive=true); filter=x->contains(x,"mnist_relu_2_512"))

    run_mnist_all("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments_final","VeryDiff-OneThread-3_100_0.49-Naive", 1.0, make_mnist_verifier_pruned("_0.49-old";naive=true); filter=x->contains(x,"mnist_relu_3_100"))

    run_mnist_all("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments_final","VeryDiff-OneThread-3_100_0.77-Naive", 1.0, make_mnist_verifier_pruned("_0.77-old";naive=true); filter=x->contains(x,"mnist_relu_3_100"))

    run_mnist_all("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments_final","VeryDiff-OneThread-3_100_0.79-Naive", 1.0, make_mnist_verifier_pruned("_0.79_adam_1_stepsize-old";naive=true); filter=x->contains(x,"mnist_relu_3_100"))

    run_mnist_all("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments_final","VeryDiff-OneThread-4_1024_0.79-Naive", 1.0, make_mnist_verifier_pruned("_0.79-old";naive=true); filter=x->contains(x,"mnist_relu_4_1024"))

    # Focused

    run_mnist_all("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments_final","VeryDiff-OneThread-Focus-2_512_0.79-Naive", 1.0, make_mnist_verifier_pruned("_0.79-old";naive=true); filter=x->contains(x,"mnist_relu_2_512"), focus=true)

    run_mnist_all("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments_final","VeryDiff-OneThread-Focus-3_100_0.49-Naive", 1.0, make_mnist_verifier_pruned("_0.49-old";naive=true); filter=x->contains(x,"mnist_relu_3_100"), focus=true)

    run_mnist_all("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments_final","VeryDiff-OneThread-Focus-3_100_0.77-Naive", 1.0, make_mnist_verifier_pruned("_0.77-old";naive=true); filter=x->contains(x,"mnist_relu_3_100"), focus=true)

    run_mnist_all("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments_final","VeryDiff-OneThread-Focus-3_100_0.79-Naive", 1.0, make_mnist_verifier_pruned("_0.79_adam_1_stepsize-old";naive=true); filter=x->contains(x,"mnist_relu_3_100"), focus=true)

    run_mnist_all("$(cur_dir)/../benchmarks/mnist-prune","$(cur_dir)/../experiments_final","VeryDiff-OneThread-Focus-4_1024_0.79-Naive", 1.0, make_mnist_verifier_pruned("_0.79-old";naive=true); filter=x->contains(x,"mnist_relu_4_1024"), focus=true)


    println("2.3) Autoencoder MNIST (Epsilon)")

    # println("2.3.1) VeryDiff")
    # run_mnist_all("$(cur_dir)/../benchmarks/mnist_autoencoders_pruned","$(cur_dir)/../experiments_final","VeryDiff-AE-1-Combined", 0.05, make_mnist_verifier_pruned("_pruned_1"))

    # run_mnist_all("$(cur_dir)/../benchmarks/mnist_autoencoders_pruned","$(cur_dir)/../experiments_final","VeryDiff-AE-5", 0.05, make_mnist_verifier_pruned("_pruned_5"))

    # run_mnist_all("$(cur_dir)/../benchmarks/mnist_autoencoders_pruned","$(cur_dir)/../experiments_final","VeryDiff-AE-10", 0.05, make_mnist_verifier_pruned("_pruned_10"))

    # run_mnist_all("$(cur_dir)/../benchmarks/mnist_autoencoders_pruned","$(cur_dir)/../experiments_final","VeryDiff-AE-16", 0.05, make_mnist_verifier_pruned("_pruned_16"))



    println("2.4) New Label MNIST (Top-1)")

    println("2.4.1) NEURODIFF")
    println("SKIPPED")

    println("2.4.2) NNEQUIV")
    run_mnist_all_top1_new("$(cur_dir)/../benchmarks/mnist-retrained-new-label","$(cur_dir)/../experiments_final","NNEquiv-New-Label",0, make_mnist_verifier_pruned_top1_nnequiv("_retrained"); timeout=120,filter=x->contains(x,"-old.onnx"), benchmark_filter=(x->(IMAGE_FOCUS[parse(Int,split(x,"_")[2])+1]<=3)))

    println("2.4.3) MILPEQUIV")
    println("SKIPPED")

    println("2.4.4) VeryDiff")
    run_mnist_all_top1_new("$(cur_dir)/../benchmarks/mnist-retrained-new-label","$(cur_dir)/../experiments_final","VeryDiff-New-Label-Combined",1, make_mnist_verifier_pruned_top1("_retrained"); timeout=120,filter=x->contains(x,"-old.onnx"), benchmark_filter=(x->(IMAGE_FOCUS[parse(Int,split(x,"_")[2])+1]<=3)))

    println("2.4.4) VeryDiff (Naive)")
    println("Skipped")
    VeryDiff.USE_DIFFZONO = false
    run_mnist_all_top1_new("$(cur_dir)/../benchmarks/mnist-retrained-new-label","$(cur_dir)/../experiments_final","VeryDiff-New-Label-Naive",1, make_mnist_verifier_pruned_top1("_retrained",naive=true); timeout=120,filter=x->contains(x,"-old.onnx"), benchmark_filter=(x->(IMAGE_FOCUS[parse(Int,split(x,"_")[2])+1]<=3)))
    VeryDiff.USE_DIFFZONO = true

    println("3) CONFIDENCE")
    println("3.1) RICE")
    println("3.1.1) VeryDiff")
    verify_rice()
    verify_rice_sigma()
    verify_rice(folder="rice-1-retrained", pruning_factors=["0.1","0.2","0.3","0.4","0.5","0.6","0.7","0.8","0.9"])
    verify_rice_sigma(folder="rice-1-retrained", pruning_factors=["0.1","0.2","0.3","0.4","0.5","0.6","0.7","0.8","0.9"])

    println("3.1) LHC")
    verify_lhc_sigma()
    
end
    
